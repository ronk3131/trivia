#pragma once

#include "sqlite3.h"
#include "LoginManager.h"
#include <map>
#include <list>

using std::map;
using std::list;

namespace callback
{
	int getInt(void *param, int argc, char **argv, char **azColName);
	int getFloat(void *param, int argc, char **argv, char **azColName);
	int getString(void *param, int argc, char **argv, char **azColName);
	int getBool(void *param, int argc, char **argv, char **azColName);
	
	int highscores(void *param, int argc, char **argv, char **azColName);
	int questions(void *param, int argc, char **argv, char **azColName);
}