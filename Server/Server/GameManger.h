#pragma once
#include "Sqlite.h"
#include "Room.h"
#include "Game.h"
#include <algorithm>

class GameManager
{
public:
	static GameManager *createObject(IDataBase*);
	static GameManager *getObject();

	GameManager(IDataBase*);
	Game *createGame(Room*);
	Game *getGame(int gameId);
	void deleteGame(int);

private:
	
	static GameManager* m_object;
	IDataBase *m_datebase;
	map<int, Game*> m_games;
};