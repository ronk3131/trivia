#pragma once
#include <iostream>
#include <map>
#include <vector>
#include "LoginManager.h"
#include "RoomData.h"
#include "PlayerResults.h"

using std::map;
using std::pair;
using std::string;
using std::vector;

typedef struct LoginResponse
{
	unsigned int status;
} LoginResponse;

typedef struct SignUpResponse
{
	unsigned int status;
} SignUpResponse;

typedef struct ErrorResponse
{
	std::string message;
} ErrorResponse;

typedef struct GetRoomsResponse
{
	unsigned int status;
	map<int, string> rooms;
} GetRoomsResponse;

typedef struct GetPlayersInRoomResponse
{
	unsigned int status;
	vector<LoggedUser> players;
} GetPlayersInRoomResponse;

typedef struct JoinLeaveRoomUpdateResponse
{
	unsigned int status;
	string playerName;
};

typedef struct HighscoreResponse
{
	unsigned int status;
	vector<Highscore> highscores;
} HighscoreResponse;

typedef struct JoinRoomResponse
{
	unsigned int status;
} JoinRoomResponse;

typedef struct KickPlayerResponse
{
	unsigned int status;
	string playerName;
};

typedef struct NewMessageResponse
{
	unsigned int status;
	string playerName;
	string message;
};

typedef struct CreateRoomResponse
{
	unsigned int status;
} CreateRoomResponse;

typedef struct GetPerformancesResponse
{
	unsigned int status;
	int gamesCount;
	int rightAnswersCount;
	int wrongAnswersCount;
	float averageTimeForAnswer;
} GetPerformancesResponse;

typedef struct CloseRoomResponse
{
	unsigned int status;
} CloseRoomResponse;

typedef struct StartGameResponse
{
	unsigned int status;
} StartGameResponse;

typedef struct GetRoomStateResponse
{
	unsigned int status;
	RoomData roomData;
	vector<string> players;
} GetRoomStateResponse;

typedef struct GetQuestionResponse
{
	unsigned int status;
	string question;
	vector<string> answers;
} GetQuestionResponse;

typedef struct SubmitAnswerResponse
{
	unsigned int status;
	int correctAnswer;
} SubmitAnswerResponse;

typedef struct GetGameResultsResponse
{
	unsigned int status;
	vector<PlayerResults> results;
} GetGameResultsResponse;