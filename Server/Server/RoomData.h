#pragma once
#include <iostream>

using std::string;

typedef struct RoomData
{
	unsigned int id;
	string name;
	unsigned int maxPlayers;
	unsigned int questionCount;
	float timePerQuestion;
	bool isActive;
} RoomData;