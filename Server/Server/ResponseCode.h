#pragma once
#include <iostream>

enum ResponseCode
{
	Error = 100,
	ProtocolFormat,
	Success = 200,
	UserTaken = 300,
	EmailTaken,
	InvalidPasswordAndUsername,
	AlreadyLoggedIn,
	InvalidUsername,
	InvalidPassword,
	InvalidEmail,
	RoomFull,
	RoomIsActive,
	InvalidRoomID,
	AlreadyInRoom,
	RoomPlayerLeaveJoin,
	RoomKickPlayer,
	CantKickAdmin,
	RoomClosed,
	GameStarted,
	InvalidCreateRoomArguementTypes,
	InvalidCreateRoomRoomName,
	InvalidCreateRoomMaxUsers,
	InvalidCreateRoomQuestionCount,
	InvalidCreateRoomAnswerTimeout,
	UserNotFound,
	SqlInjection,
	GameOver,
	WaitForPlayersToAnswer,
	PlayerNotFinished,
	AlreadySubmitted,
	QuestionNotAnswered,
	NewMessageUpdate
};

class ResponseMessages
{
public:
	static std::string getMessage(ResponseCode code);
};