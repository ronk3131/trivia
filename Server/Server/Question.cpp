#include "Question.h"

Question::Question(string question, string answer0, string answer1, string answer2, string answer3, int correctAnswer) : m_question(question), m_correctAnswer(correctAnswer)
{
	m_possibleAnswers.push_back(answer0);
	m_possibleAnswers.push_back(answer1);
	m_possibleAnswers.push_back(answer2);
	m_possibleAnswers.push_back(answer3);
}

Question::Question()
{
}

string Question::getQuestion()
{
	return m_question;
}

vector<string> Question::getPossibleAnswers()
{
	return m_possibleAnswers;
}

int Question::getCorrectAnswer()
{
	return m_correctAnswer;
}

void Question::setQuestion(string question)
{
	m_question = question;
}

void Question::setPossibleAnswer(string answer)
{
	m_possibleAnswers.push_back(answer);
}

void Question::setCorrectAnswer(int correctAnswer)
{
	m_correctAnswer = correctAnswer;
}

void Question::clearPossibleAnswers()
{
	m_possibleAnswers.clear();
}
