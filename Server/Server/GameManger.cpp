#include "GameManger.h"

GameManager *GameManager::m_object = NULL;
GameManager *GameManager::createObject(IDataBase *db)
{
	if (m_object)
	{
		throw std::exception("GameManager object has already been created");
	}

	m_object = new GameManager(db);
	return m_object;
}

GameManager *GameManager::getObject()
{
	if (!m_object)
	{
		throw std::exception("GameManager object must be created first.");
	}
	return m_object;
}

GameManager::GameManager(IDataBase *db) : m_datebase(db)
{
}

Game *GameManager::createGame(Room* room)
{
	Game *game = new Game(room->getMetadata().id, m_datebase->getQuestions(room->getMetadata().questionCount), room->getAllUsers());
	m_games[game->getGameId()] = game;
	return game;
}
Game *GameManager::getGame(int gameId)
{
	return m_games[gameId];
}

void GameManager::deleteGame(int gameId)
{
	m_games.erase(gameId);
}
