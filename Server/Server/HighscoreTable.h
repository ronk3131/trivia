#pragma once
#include "Sqlite.h"
#include <vector>

using std::vector;

class HighscoreTable
{
public:
	static HighscoreTable *createObject(IDataBase*);
	static HighscoreTable *getObject();

	vector<Highscore> getHighscores();

private:
	HighscoreTable(IDataBase *db);

	static HighscoreTable* m_object;

	IDataBase *m_database;
};