#pragma once
#include "IRequestHandler.h"
#include "LoginManager.h"
#include "Sqlite.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"

class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(RequestHandlerFactory*, LoginManager*);
	~LoginRequestHandler();

	bool isRequestRelevant(Request request) override;
	RequestResult *handleRequest(Request request) override;

private:
	LoginManager *m_loginManager;
	RequestHandlerFactory* m_handlerFactory;
	RequestResult *login(Request);
	RequestResult *signup(Request);
};