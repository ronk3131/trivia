#include "RoomRequestHandler.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(RequestHandlerFactory *handlerFactory, Room *room, LoggedUser user, LoginManager *loginManager, RoomManager *roomManager) : m_handlerFactory(handlerFactory), m_room(room), m_user(user), m_loginManager(loginManager), m_roomManager(roomManager)
{
}

bool RoomMemberRequestHandler::isRequestRelevant(Request request)
{
	switch (request.id)
	{
	case RequestCode::GetRoomState:
	case RequestCode::LeaveRoom:
		return true;
	case RequestCode::RoomSendMessage:
		return request.buffer != NULL;
	default:
		return false;
	}
}

RequestResult *RoomMemberRequestHandler::handleRequest(Request request)
{
	switch (request.id)
	{
		case RequestCode::GetRoomState: return getRoomState(request);
		case RequestCode::LeaveRoom: return leaveRoom(request);
		case RequestCode::RoomSendMessage: return sendMessage(request);
	}
	return NULL;
}

RequestResult *RoomMemberRequestHandler::getRoomState(Request request)
{
	RequestResult *result = new RequestResult;

	GetRoomStateResponse getRoomStateRes;

	//init getRoomState response
	getRoomStateRes.players = m_room->getAllUsersName();
	getRoomStateRes.roomData = m_room->getMetadata();
	getRoomStateRes.status = ResponseCode::Success;

	//serialize getRoomState response
	char* serialRes = JsonResponsePacketSerializer::serializeResponse(getRoomStateRes);

	//init result
	result->response = Communicator::createResponse(getRoomStateRes.status, serialRes);
	result->newHandler = this;

	return result;
}

RequestResult * RoomMemberRequestHandler::sendMessage(Request request)
{
	RequestResult *result = new RequestResult;
	try
	{
		SendMessageRequest req = JsonRequestPacketDeserializer::deserializeSendMessageRequest(request.buffer);
		delete result;
		result = new NewMessageRequestUpdate();
		NewMessageRequestUpdate *newMessageUpdate = (NewMessageRequestUpdate*)result;
		newMessageUpdate->updateRoomType = ResponseCode::NewMessageUpdate;
		newMessageUpdate->message = req.message;
		newMessageUpdate->playerName = m_user.getUsername();
		newMessageUpdate->sockets = m_room->getAllSockets();
	}
	catch (std::exception)
	{
		result->response = Communicator::createResponse(ResponseCode::Error);
	}
	result->newHandler = this;
	return result;
}

void RoomMemberRequestHandler::forceSignout()
{
	m_loginManager->logout(m_user.getUsername());
	m_room->removeUser(m_user);
}

LoggedUser RoomMemberRequestHandler::getUser()
{
	return m_user;
}

Room * RoomMemberRequestHandler::getRoom()
{
	return m_room;
}

RequestResult *RoomMemberRequestHandler::leaveRoom(Request request)
{
	JoinLeaveRequestUpdate *result = new JoinLeaveRequestUpdate;

	//erase user from room
	m_room->removeUser(m_user);
	
	//init result
	result->response = NULL;
	result->newHandler = m_handlerFactory->createMenuRequestHandler(m_user.getUsername());
	result->sockets = m_room->getAllSockets();
	result->playerName = m_user.getUsername();
	
	result->updateRoomType = ResponseCode::RoomPlayerLeaveJoin;

	return result;
}