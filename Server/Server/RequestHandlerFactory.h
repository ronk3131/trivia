#pragma once
#include "LoginRequestHandler.h"
#include "RoomManager.h"
#include "HighscoreTable.h"
#include "MenuRequestHandler.h"
#include "RoomRequestHandler.h"
#include "GameRequestHandler.h"

class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;
class GameManager;

class RequestHandlerFactory
{
public:
	static RequestHandlerFactory *createObject(LoginManager*, RoomManager*, GameManager*, HighscoreTable*, IDataBase*);
	static RequestHandlerFactory *getObject();

	~RequestHandlerFactory();
	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(LoggedUser);
	RoomAdminRequestHandler* createRoomAdminRequestHandler(LoggedUser, Room*);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(LoggedUser, Room*);
	GameRequestHandler* createGameRequestHandler(LoggedUser, Room*);
	GameRequestHandler* createGameRequestHandler(LoggedUser, int);

private:
	RequestHandlerFactory(LoginManager*, RoomManager*, GameManager*, HighscoreTable*, IDataBase*);

	static RequestHandlerFactory *m_object;

	LoginManager *m_loginManager;
	RoomManager *m_roomManager;
	HighscoreTable *m_highscoreTable;
	GameManager *m_gameManager;
	IDataBase *m_database;
};