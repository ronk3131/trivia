#pragma once
#include "IRequestHandler.h"
#include "Room.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include "Communicator.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;

class RoomMemberRequestHandler : public IRequestHandler
{
public:
	RoomMemberRequestHandler(RequestHandlerFactory*, Room*, LoggedUser, LoginManager*, RoomManager*);
	bool isRequestRelevant(Request) override;
	RequestResult *handleRequest(Request) override;
	void forceSignout() override;
	LoggedUser getUser();
	Room *getRoom();

protected:
	Room *m_room;
	LoggedUser m_user;
	LoginManager *m_loginManager;
	RoomManager *m_roomManager;
	RequestHandlerFactory *m_handlerFactory;
	virtual RequestResult *leaveRoom(Request);
	RequestResult *getRoomState(Request);
	RequestResult *sendMessage(Request);
};

class RoomAdminRequestHandler : public RoomMemberRequestHandler
{
public:
	RoomAdminRequestHandler(RequestHandlerFactory*, Room*, LoggedUser, LoginManager*, RoomManager*);
	bool isRequestRelevant(Request) override;
	RequestResult *handleRequest(Request) override;
	void closeRoom();
	
private:
	RequestResult *leaveRoom(Request) override;
	RequestResult *startGame(Request);
	RequestResult *kickPlayer(Request);
};