#pragma once
#include "Response.h"
#include "json.hpp"

#pragma warning(disable:4996)

using json = nlohmann::json;

class JsonResponsePacketSerializer
{
public:
	static char* serializeResponse(ErrorResponse);
	static char* serializeResponse(LoginResponse);
	static char* serializeResponse(SignUpResponse);
	static char* serializeResponse(GetRoomsResponse);
	static char* serializeResponse(GetPlayersInRoomResponse);
	static char* serializeResponse(JoinRoomResponse);
	static char* serializeResponse(JoinLeaveRoomUpdateResponse);
	static char* serializeResponse(KickPlayerResponse);
	static char* serializeResponse(NewMessageResponse);
	static char* serializeResponse(CreateRoomResponse);
	static char* serializeResponse(HighscoreResponse);
	static char* serializeResponse(GetPerformancesResponse);
	static char* serializeResponse(GetRoomStateResponse);
	static char* serializeResponse(GetQuestionResponse);
	static char* serializeResponse(SubmitAnswerResponse);
	static char* serializeResponse(GetGameResultsResponse);

private:
	static char* jsonToCharArray(json);
};