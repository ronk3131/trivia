#include "sql_callbacks.h"
#include <iostream>
#include <list>
#include <string>

#pragma warning(disable:4996)

namespace callback
{
	int getInt(void *param, int argc, char **argv, char **azColName)
	{
		if (argc != 0)
		{
			int **res = (int **)param;
			**res = argv[0] ? atoi(argv[0]) : 0;
		}
		return 0;
	}

	int getFloat(void *param, int argc, char **argv, char **azColName)
	{
		if (argc != 0)
		{
			float **res = (float **)param;
			**res = argv[0] ? atof(argv[0]) : 0;
		}
		return 0;
	}

	int getString(void *param, int argc, char **argv, char **azColName)
	{
		if (argc != 0)
		{
			char **res = (char **)param;
			*res = new char[sizeof(argv[0]) + 1];
			strcpy(*res, argv[0]);
		}
		return 0;
	}

	int getBool(void *param, int argc, char **argv, char **azColName)
	{
		if (argc != 0)
		{
			bool **res = (bool **)param;
			**res = atoi(argv[0]) > 0;
		}
		return 0;
	}
	int highscores(void * param, int argc, char ** argv, char ** azColName)
	{
		map<string, int> *db_highscores = static_cast<map<string, int>*>(param);
		
		db_highscores->insert(std::pair<string, int>(argv[0], atoi(argv[1])));

		return 0;
	}
	int questions(void * param, int argc, char ** argv, char ** azColName)
	{
		vector<Question*> *db_questions = static_cast<vector<Question*>*>(param);		
		
		db_questions->push_back(new Question(argv[0], argv[1], argv[2], argv[3], argv[4], atoi(argv[5])));

		return 0;
	}
}