#pragma once
#include <time.h>
#include "ResponseCode.h"
#include <vector>
#include "LoggedUser.h"
#include <WinSock2.h>
#include <Windows.h>
#include "Room.h"

using std::vector;

typedef struct Request
{
	int id; // request type.
	time_t receivalTime;
	char* buffer;
}Request;

typedef struct RequestResult RequestResult;

class IRequestHandler
{
public:
	~IRequestHandler() = default;
	virtual bool isRequestRelevant(Request request) = 0;
	virtual RequestResult *handleRequest(Request request) = 0;
	virtual void forceSignout();
};

typedef struct RequestResult
{
	virtual ~RequestResult() {}; // make this a polymorphic struct
	char* response;
	IRequestHandler* newHandler;
}RequestResult;

typedef struct RoomChangeRequestUpdate : RequestResult
{
	ResponseCode updateRoomType;
	vector<SOCKET> sockets;
};

typedef struct JoinLeaveRequestUpdate : RoomChangeRequestUpdate
{
	string playerName;
};

typedef struct KickPlayerRequestUpdate : RoomChangeRequestUpdate
{
	string playerName;
	SOCKET playerSocket;
};

typedef struct NewMessageRequestUpdate : RoomChangeRequestUpdate
{
	string playerName;
	string message;
};

typedef struct StartGameRequestUpdate : RoomChangeRequestUpdate
{
	int gameId;
	vector<LoggedUser> users;
};
typedef struct RoomClosedRequestUpdate : RoomChangeRequestUpdate
{
	vector<LoggedUser> users;
};
