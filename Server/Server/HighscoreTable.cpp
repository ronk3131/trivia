#include "HighscoreTable.h"

HighscoreTable::HighscoreTable(IDataBase * db) : m_database(db)
{
}

HighscoreTable *HighscoreTable::m_object = NULL;
HighscoreTable *HighscoreTable::createObject(IDataBase *db)
{
	if (m_object)
	{
		throw std::exception("HighscoreTable object has already been created");
	}

	m_object = new HighscoreTable(db);
	return m_object;
}
HighscoreTable * HighscoreTable::getObject()
{
	if (!m_object)
	{
		throw std::exception("HighscoreTable object must be created first.");
	}
	return m_object;
}


vector<Highscore> HighscoreTable::getHighscores()
{
	vector<Highscore> highScores;
	map<string, int> db_highscores = m_database->getHighScores();

	for (map<string, int>::iterator it = db_highscores.begin(); it != db_highscores.end(); ++it)
	{
		Highscore highscore = { it->first, it->second };
		highScores.push_back(highscore);
	}

	return highScores;
}
