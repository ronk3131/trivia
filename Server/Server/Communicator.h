#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <map>
#include <thread>
#include "RequestHandlerFactory.h"
#include "JsonResponsePacketSerializer.h"
#include <sstream>
#include <iomanip>

using std::map;
using std::thread;

class Communicator
{
public:
	Communicator(RequestHandlerFactory*);
	~Communicator();

	void bindAndListen(int port);
	void handleRequests(char*, SOCKET);
	static char *createResponse(int, char*);
	static char *createResponse(int);
	void updateUsersInRoom(RoomChangeRequestUpdate *);

private:
	void startThreadForNewClient(char*, SOCKET);
	void sendError(SOCKET client_socket);
	
	void joinOrLeaveUpdate(JoinLeaveRequestUpdate*);
	void closeRoomUpdate(RoomClosedRequestUpdate*);
	void startGameUpdate(StartGameRequestUpdate*);
	void kickPlayerUpdate(KickPlayerRequestUpdate*);
	void newMessageUpdate(NewMessageRequestUpdate*);

	map<SOCKET, IRequestHandler*> m_clients;
	vector<thread> m_clientThreads;
	RequestHandlerFactory *m_handlerFactory;
	SOCKET m_serverSocket;
};