#pragma once

#include <iostream>

typedef struct PlayerResults
{
	std::string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;

	bool operator<(const PlayerResults& a) const
	{
		return correctAnswerCount > a.correctAnswerCount;
	}
} PlayerResults;