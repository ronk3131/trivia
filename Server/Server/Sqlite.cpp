#include "Sqlite.h"
#include <fstream>

using std::ifstream;

Sqlite::Sqlite()
{
	std::string dbFilename = "triviaDB.sqlite";

	int doesFileExist = _access(dbFilename.c_str(), 0);
	if (sqlite3_open(dbFilename.c_str(), &m_db) != SQLITE_OK)
	{
		m_db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		exit(1);
	}

	if (doesFileExist == -1)
	{
		std::string createTablesQueries[] =
		{
			"CREATE TABLE USERS (username TEXT PRIMARY KEY NOT NULL, email TEXT NOT NULL, password TEXT NOT NULL)",
			"CREATE TABLE USER_STATISTICS (username TEXT PRIMARY KEY NOT NULL, gamesCount INTEGER NOT NULL DEFAULT 0, rightAnswersCount INTEGER NOT NULL DEFAULT 0, wrongAnswersCount INTEGER NOT NULL DEFAULT 0, averageTimeForAnswer FLOAT NOT NULL DEFAULT 0.0, bestScore INTEGER NOT NULL DEFAULT 0)",
			"CREATE TABLE QUESTIONS (question TEXT NOT NULL UNIQUE, answer0 TEXT NOT NULL, answer1 TEXT NOT NULL, answer2 TEXT NOT NULL, answer3 TEXT NOT NULL, correctAnswer INTEGER)"
		};

		char *errMessage = NULL;

		for (int i = 0; i < size(createTablesQueries); i++)
		{
			if (sqlite3_exec(m_db, createTablesQueries[i].c_str(), nullptr, nullptr, &errMessage) != SQLITE_OK)
			{
				remove(dbFilename.c_str());
				std::cout << errMessage << std::endl;
				system("pause");
				exit(1);
			}
		}
	}
}


Sqlite::~Sqlite()
{
	sqlite3_close(m_db);
	m_db = nullptr;
}

Sqlite *Sqlite::m_object = NULL;
Sqlite *Sqlite::getObject()
{
	if (!m_object)
	{
		m_object = new Sqlite();
	}
	return m_object;
}

map<string, int> Sqlite::getHighScores()
{
	map<string, int> highscores;

	string query = "SELECT username, bestScore FROM USER_STATISTICS ORDER BY bestScore DESC;";

	char *errMessage;
	if (sqlite3_exec(m_db, query.c_str(), &callback::highscores, &highscores, &errMessage) != SQLITE_OK)
	{
		throw std::exception(errMessage);
	}

	return highscores;
}

void Sqlite::addUser(string username, string email, string password)
{
	execute("INSERT INTO USERS (username, email, password) VALUES ('" + username + "', '" + email + "', '" + password + "');");
	execute("INSERT INTO USER_STATISTICS (username) VALUES ('" + username + "');");
}

bool Sqlite::doesUsernameTaken(string username)
{
	vector<string> stringsToCheck = { username };
	if (isProtectedFromSqlInjection(stringsToCheck))
	{
		return execute<bool>("SELECT Count() FROM USERS WHERE username = '" + username + "';");
	}
	throw SqlException();
}

bool Sqlite::doesEmailTaken(string email)
{
	vector<string> stringsToCheck = { email };
	if (isProtectedFromSqlInjection(stringsToCheck))
	{
		return execute<bool>("SELECT Count() FROM USERS WHERE email = '" + email + "';");
	}
	throw SqlException();
}

bool Sqlite::login(string username, string password)
{
	vector<string> stringsToCheck = { username, password };
	if (isProtectedFromSqlInjection(stringsToCheck))
	{
		return execute<bool>("SELECT Count() FROM USERS WHERE username = '" + username + "' AND password = '" + password + "';");
	}
	throw SqlException();
}

int Sqlite::getGameCountOfPlayer(string username)
{
	return execute<int>("Select gamesCount FROM USER_STATISTICS WHERE username = '" + username + "';");
}

int Sqlite::getRightAnswerCountOfPlayer(string username)
{
	return execute<int>("Select rightAnswersCount FROM USER_STATISTICS WHERE username = '" + username + "';");
}

int Sqlite::getWrongAnswersCountOfPlayer(string username)
{
	return execute<int>("Select wrongAnswersCount FROM USER_STATISTICS WHERE username = '" + username + "';");
}

float Sqlite::getAvergaeTimeForAnswerOfPlayer(string username)
{
	return execute<float>("Select averageTimeForAnswer FROM USER_STATISTICS WHERE username = '" + username + "';");
}

vector<Question*> Sqlite::getQuestions(int numberOfQuestions)
{
	vector<Question*> questions;
	string query = "SELECT * FROM QUESTIONS ORDER BY RANDOM() LIMIT " + std::to_string(numberOfQuestions) + ";";
	char *errMessage;

	if (sqlite3_exec(m_db, query.c_str(), &callback::questions, &questions, &errMessage) != SQLITE_OK)
	{
		throw std::exception(errMessage);
	}

	return questions;
}

int Sqlite::getQuestionsCount()
{
	return execute<int>("Select Count() FROM QUESTIONS;");
}

void Sqlite::updatePlayersResults(vector<PlayerResults> results)
{
	int currentGameQuestionsCount = results[0].correctAnswerCount + results[0].wrongAnswerCount;
	string query;

	for (PlayerResults result : results)
	{
		query = "UPDATE USER_STATISTICS ";
		query += "SET ";
		query += "gamesCount = gamesCount + 1,";
		query += "averageTimeForAnswer = (averageTimeForAnswer*(rightAnswersCount + wrongAnswersCount) + " + std::to_string(result.averageAnswerTime) + "*" + std::to_string(currentGameQuestionsCount) + ") / (rightAnswersCount + wrongAnswersCount + " + std::to_string(currentGameQuestionsCount) + "),";
		query += "rightAnswersCount = rightAnswersCount + " + std::to_string(result.correctAnswerCount) + ",";
		query += "wrongAnswersCount = wrongAnswersCount + " + std::to_string(result.wrongAnswerCount) + ",";
		query += "bestScore = 0.5 * ((bestScore + " + std::to_string(result.correctAnswerCount) + ") + ABS(bestScore - " + std::to_string(result.correctAnswerCount) + ")) ";
		query += "WHERE username = '" + result.username + "';";
		execute(query);
	}
}

template<class T>
T Sqlite::execute(string query)
{
	char *errMessage;
	
	int(*callbackFunc)(void *, int, char**, char**);
	if (typeid(T) == typeid(bool))
	{
		callbackFunc = callback::getBool;
	}
	else if (typeid(T) == typeid(int))
	{
		callbackFunc = callback::getInt;
	}
	else if (typeid(T) == typeid(float))
	{
		callbackFunc = callback::getFloat;
	}
	else if (typeid(T) == typeid(string))
	{
		callbackFunc = callback::getString;
	}
	else
	{
		throw std::exception("The requested type from sql is not supported");
	}

	T *param = new T;

	if (sqlite3_exec(m_db, query.c_str(), callbackFunc, &param, &errMessage) != SQLITE_OK)
	{
		delete param;
		throw std::exception(errMessage);
	}

	T retValue = *param;
	delete param;

	return retValue;
}

void Sqlite::execute(string query)
{
	char *errMessage;	
	if (sqlite3_exec(m_db, query.c_str(), NULL, NULL, &errMessage) != SQLITE_OK)
	{
		throw std::exception(errMessage);
	}
}

const string Sqlite::ILLEGAL_CHARS = ",;\'\"`";
bool Sqlite::isProtectedFromSqlInjection(vector<string> strings)
{
	for (string str : strings)
	{
		if (str.find_first_of(ILLEGAL_CHARS) != std::string::npos)
		{
			return false;
		}
	}

	return true;
}
