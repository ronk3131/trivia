#pragma once
#include <iostream>

using std::string;

enum RequestCode
{
	None,
	Logout,
	Login,
	Signup,
	GetRooms,
	GetPlayersInRoom,
	JoinRoom,
	CreateRoom,
	MyPerformances,
	BestScores,
	StartGame,
	KickPlayer,
	GetRoomState,
	LeaveRoom,
	GetQuestion,
	SubmitAnswer,
	GetGameResults,
	LeaveGame,
	RoomSendMessage
};

typedef struct LoginRequest
{
	string username;
	string password;
} LoginRequest;

typedef struct SignUpRequest
{
	string username;
	string password;
	string email;
} SignUpRequest;

typedef struct GetPlayersInRoomRequest
{
	unsigned int roomId;
} GetPlayersInRoomRequest;

typedef struct JoinRoomRequest
{
	unsigned int roomId;
} JoinRoomRequest;

typedef struct KickPlayerRequest
{
	string playerName;
} KickPlayerRequest;

typedef struct SendMessageRequest
{
	string message;
};

typedef struct CreateRoomRequest
{
	string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	float answerTimeout;
} CreateRoomRequest;

typedef struct SubmitAnswerRequest
{
	int answerId;
} SubmitAnswerRequest;