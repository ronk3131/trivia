#include <iostream>
#include "Server.h"
#include "WSAInitializer.h"

#define SERVER_PORT 8820

int main()
{
	try 
	{
		WSAInitializer wsaInit;

		Server server(SERVER_PORT, Sqlite::getObject());
		server.run();
	}

	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
	}

	system("pause");
	return 0;
}