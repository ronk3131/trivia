#include "SqlException.h"

const char * SqlException::what() const throw()
{
	return "SqlError: Someone tried to break the data base of the server using sql injection";
}
