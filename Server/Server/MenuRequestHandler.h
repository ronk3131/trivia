#pragma once
#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "RequestHandlerFactory.h"
#include "HighscoreTable.h"
#include "RoomRequestHandler.h"

class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(RequestHandlerFactory*, LoginManager*, RoomManager*, LoggedUser, HighscoreTable*, IDataBase*);
	~MenuRequestHandler();

	bool isRequestRelevant(Request) override;
	RequestResult *handleRequest(Request) override;
	void forceSignout() override;
	LoggedUser getUser();
	
private:
	LoggedUser m_user;
	RoomManager *m_roomManager;
	LoginManager *m_loginManager;
	HighscoreTable *m_highscoreTable;
	IDataBase *m_database;
	RequestHandlerFactory *m_handlerFactory;
	
	RequestResult *signout();
	RequestResult *getRooms(Request);
	RequestResult *getPlayersInRoom(Request);
	RequestResult *getHighscores(Request);
	RequestResult *joinRoom(Request);
	RequestResult *createRoom(Request);
	RequestResult *getPerformances(Request);
};