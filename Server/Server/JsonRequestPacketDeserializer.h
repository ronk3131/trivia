#pragma once
#include "Request.h"

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(char*);
	static SignUpRequest deserializeSignUpRequest(char*);
	static GetPlayersInRoomRequest deserializeGetPlayersInRoomRequest(char*);
	static JoinRoomRequest deserializeJoinRoomRequest(char*);
	static KickPlayerRequest deserializeKickPlayerRequest(char*);
	static SendMessageRequest deserializeSendMessageRequest(char*);
	static CreateRoomRequest deserializeCreateRoomRequest(char*);
	static SubmitAnswerRequest deserializeSubmitAnswerRequest(char*);
};
