#pragma once
#include "RequestHandlerFactory.h"
#include "Communicator.h"
#include "Sqlite.h"

class Server
{
public:
	Server(int port, IDataBase *db);
	~Server();
	void run();

private:
	IDataBase *m_database;
	Communicator *m_communicator;
	RequestHandlerFactory *m_handlerFactory;
	int m_port;

};