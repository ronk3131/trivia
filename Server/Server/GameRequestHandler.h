#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "GameManger.h"
#include "Game.h"

class GameRequestHandler : public IRequestHandler
{
public:
	GameRequestHandler(RequestHandlerFactory*, Room*, GameManager*, LoginManager*, IDataBase*, LoggedUser);
	GameRequestHandler(RequestHandlerFactory*, int, GameManager*, LoginManager*, IDataBase*, LoggedUser);
	void forceSignout() override;
	bool isRequestRelevant(Request);
	RequestResult *handleRequest(Request) override;

private:
	Game *m_game;
	LoggedUser m_user;
	IDataBase *m_database;
	GameManager *m_gameMananger;
	LoginManager *m_loginManager;
	RequestHandlerFactory *m_handlerFactory;
	time_t questionStartTime;
	bool isAlreadySubmitted;

	RequestResult *getQuestion(Request);
	RequestResult *submitAnswer(Request);
	RequestResult *getGameResults(Request);
	RequestResult *leaveGame(Request);
};