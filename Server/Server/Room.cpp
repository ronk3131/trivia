#include "Room.h"
#include <algorithm>
#include "Communicator.h"

Room::Room()
{
}

Room::Room(RoomData roomData) : m_metadata(roomData)
{
}

Room::~Room()
{
	for (vector<LoggedUser>::iterator userIt = m_users.begin(); userIt != m_users.end();)
	{
		m_sockets.erase(std::remove(m_sockets.begin(), m_sockets.end(), m_usersSockets[userIt->getUsername()]), m_sockets.end());
		m_usersSockets.erase(userIt->getUsername());
		userIt = m_users.erase(userIt);
	}
}

ResponseCode Room::addUser(LoggedUser user)
{
	if (m_metadata.isActive)
	{
		return ResponseCode::RoomIsActive;
	}

	if (m_metadata.maxPlayers == m_users.size())
	{
		return ResponseCode::RoomFull;
	}
	if (hasUser(user))
	{
		return ResponseCode::AlreadyInRoom;
	}

	m_users.push_back(user);
	// communicator will add the sockets later.

	return ResponseCode::Success;
}

void Room::removeUser(LoggedUser user)
{
	for (vector<LoggedUser>::iterator userIt = m_users.begin(); userIt != m_users.end();)
	{
		if (userIt->getUsername() == user.getUsername())
		{
			m_sockets.erase(std::remove(m_sockets.begin(), m_sockets.end(), m_usersSockets[userIt->getUsername()]), m_sockets.end());
			m_usersSockets.erase(userIt->getUsername());
			userIt = m_users.erase(userIt);
		}
		else
		{
			userIt++;
		}
	}
}

bool Room::hasUser(LoggedUser user)
{
	return std::find_if(m_users.begin(), m_users.end(), [&user](LoggedUser iter) { return iter.getUsername() == user.getUsername(); }) != m_users.end();
}

SOCKET Room::getSocket(LoggedUser user)
{
	return m_usersSockets[user.getUsername()];
}

vector<LoggedUser> Room::getAllUsers()
{
	return m_users;
}

vector<SOCKET> Room::getAllSockets()
{
	return m_sockets;
}

vector<string> Room::getAllUsersName()
{
	vector<string> players;

	for (vector<LoggedUser>::iterator player = m_users.begin(); player != m_users.end(); player++)
	{
		players.push_back(player->getUsername());
	}

	return players;
}

RoomData Room::getMetadata()
{
	return m_metadata;
}

void Room::addSocket(string username,  SOCKET client_socket)
{
	m_sockets.push_back(client_socket);
	m_usersSockets[username] = client_socket;
}

void Room::startGame()
{
	m_metadata.isActive = true;
}
