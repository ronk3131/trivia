#include "Server.h"

Server::Server(int port, IDataBase *db) : m_port(port), m_database(db)
{
	LoginManager *loginManager = LoginManager::createObject(db);
	RoomManager *roomManager = RoomManager::createObject(db);
	GameManager *gameManager = GameManager::createObject(db);
	HighscoreTable *highscoreTable = HighscoreTable::createObject(db);
	m_handlerFactory = RequestHandlerFactory::createObject(loginManager, roomManager, gameManager, highscoreTable, db);
	m_communicator = new Communicator(m_handlerFactory);
}

Server::~Server()
{
	delete m_communicator;
	delete m_database;
	delete m_handlerFactory;
}

void Server::run()
{
	m_communicator->bindAndListen(m_port);
}
