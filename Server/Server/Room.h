#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <vector>
#include <map>
#include "RoomData.h"
#include "LoggedUser.h"
#include "ResponseCode.h"

using std::vector;
using std::map;

class Room
{
public:
	Room();
	Room(RoomData);
	~Room();

	ResponseCode addUser(LoggedUser);
	void removeUser(LoggedUser);
	bool hasUser(LoggedUser);
	SOCKET getSocket(LoggedUser);
	vector<LoggedUser> getAllUsers();
	vector<SOCKET> getAllSockets();
	vector<string> getAllUsersName();
	RoomData getMetadata();
	void addSocket(string, SOCKET);
	void startGame();

private:
	RoomData m_metadata;
	vector<LoggedUser> m_users;
	vector<SOCKET> m_sockets;
	map<string, SOCKET> m_usersSockets;
};