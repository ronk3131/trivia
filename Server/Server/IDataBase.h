#pragma once
#include <map>
#include <list>
#include "LoggedUser.h"
#include "Question.h"
#include "PlayerResults.h"

using std::map;
using std::list;

class IDataBase
{
public:
	virtual map<string, int> getHighScores() = 0;
	virtual bool doesUsernameTaken(string) = 0;
	virtual bool doesEmailTaken(string) = 0;
	virtual void addUser(string, string, string) = 0;
	virtual bool login(string, string) = 0;
	virtual int getGameCountOfPlayer(string) = 0;
	virtual int getRightAnswerCountOfPlayer(string) = 0;
	virtual int getWrongAnswersCountOfPlayer(string) = 0;
	virtual float getAvergaeTimeForAnswerOfPlayer(string) = 0;
	virtual vector<Question*> getQuestions(int) = 0;
	virtual int getQuestionsCount() = 0;
	virtual void updatePlayersResults(vector<PlayerResults>) = 0;
};