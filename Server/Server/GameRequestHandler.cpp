#include "GameRequestHandler.h"

GameRequestHandler::GameRequestHandler(RequestHandlerFactory *handlerFactory, Room *room, GameManager *gameManager, LoginManager *loginManager, IDataBase *database, LoggedUser user) : m_handlerFactory(handlerFactory), m_gameMananger(gameManager), m_loginManager(loginManager), m_database(database), m_user(user), isAlreadySubmitted(true)
{
	m_game = m_gameMananger->createGame(room);
}
GameRequestHandler::GameRequestHandler(RequestHandlerFactory *handlerFactory, int gameId, GameManager *gameManager, LoginManager *loginManager, IDataBase *database, LoggedUser user) : m_handlerFactory(handlerFactory), m_gameMananger(gameManager), m_loginManager(loginManager), m_database(database), m_user(user), isAlreadySubmitted(true)
{
	m_game = m_gameMananger->getGame(gameId);
}

void GameRequestHandler::forceSignout()
{
	if (!m_game->removePlayer(m_user))
	{
		m_gameMananger->deleteGame(m_game->getGameId());
	}
	m_loginManager->logout(m_user.getUsername());
}

bool GameRequestHandler::isRequestRelevant(Request request)
{
	switch (request.id)
	{
		case RequestCode::GetQuestion:
		case RequestCode::GetGameResults:
		case RequestCode::LeaveGame:
			return true;
		case RequestCode::SubmitAnswer:
			return request.buffer != NULL;
		default:
			false;
	}
}

RequestResult *GameRequestHandler::handleRequest(Request request)
{
	switch (request.id)
	{
		case RequestCode::GetQuestion: return getQuestion(request);
		case RequestCode::GetGameResults: return getGameResults(request);
		case RequestCode::LeaveGame: return leaveGame(request);
		case RequestCode::SubmitAnswer: return submitAnswer(request);
	}
}

RequestResult *GameRequestHandler::getQuestion(Request request)
{
	RequestResult *result = new RequestResult;

	GetQuestionResponse getQuestionRes;

	if (!isAlreadySubmitted)
	{
		result->response = Communicator::createResponse(ResponseCode::QuestionNotAnswered);
	}
	else if (m_game->isOver())
	{
		result->response = Communicator::createResponse(ResponseCode::GameOver);
	}
	else
	{
		Question *nextQuestion = m_game->getQuestion(m_user);
		getQuestionRes.status = ResponseCode::Success;
		getQuestionRes.question = nextQuestion->getQuestion();
		getQuestionRes.answers = nextQuestion->getPossibleAnswers();
		result->response = Communicator::createResponse(getQuestionRes.status, JsonResponsePacketSerializer::serializeResponse(getQuestionRes));
		questionStartTime = request.receivalTime;
		isAlreadySubmitted = false;
	}
	result->newHandler = this;
	
	return result;
}

RequestResult *GameRequestHandler::submitAnswer(Request request)
{
	RequestResult *result = new RequestResult;

	if (m_game->isOver())
	{
		result->response = Communicator::createResponse(ResponseCode::GameOver);
	}
	else if (isAlreadySubmitted)
	{
		result->response = Communicator::createResponse(ResponseCode::AlreadySubmitted);
	}
	else
	{
		SubmitAnswerRequest submitAnswerReq = JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(request.buffer);
		SubmitAnswerResponse submitAnswerRes{ ResponseCode::Success, m_game->getCorrectAnswer(m_user) };

		m_game->submitAnswer(m_user, difftime(request.receivalTime, questionStartTime), submitAnswerReq.answerId);
		isAlreadySubmitted = true;

		result->response = Communicator::createResponse(submitAnswerRes.status, JsonResponsePacketSerializer::serializeResponse(submitAnswerRes));
	}
	result->newHandler = this;

	return result;
}

RequestResult *GameRequestHandler::getGameResults(Request)
{
	RequestResult *result = new RequestResult;

	if (m_game->isPlayerFinished(m_user))
	{
		GetGameResultsResponse gameResultRes;

		gameResultRes.status = ResponseCode::Success;
		gameResultRes.results = m_game->getResults();

		result->response = Communicator::createResponse(gameResultRes.status, JsonResponsePacketSerializer::serializeResponse(gameResultRes));
	}
	else
	{
		result->response = Communicator::createResponse(ResponseCode::PlayerNotFinished);
	}
	result->newHandler = this;
	return result;
}

RequestResult *GameRequestHandler::leaveGame(Request)
{
	RequestResult *result = new RequestResult;

	// this return how many players left in the game, after removing this player.
	if (!m_game->removePlayer(m_user))
	{
		m_database->updatePlayersResults(m_game->getResults());
		m_gameMananger->deleteGame(m_game->getGameId());
	}

	result->newHandler = m_handlerFactory->createMenuRequestHandler(m_user);
	result->response = NULL;

	return result;
}
