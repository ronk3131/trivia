#include "Game.h"
#include <algorithm>

Game::Game(int gameId, vector<Question*> questions, vector<LoggedUser> players) : m_gameId(gameId), m_questions(questions), m_currentQuestionSubmits(0), m_currentQuestionNumber(0)
{
	for (LoggedUser player : players)
	{
		m_players[player.getUsername()] = PlayerResults{ player.getUsername(), 0, 0, 0 };
	}
}

Question *Game::getQuestion(LoggedUser user)
{
	std::unique_lock<mutex> ulock(waitForPlayersMtx);
	while(getCurrentQuestionNumber(user) != m_currentQuestionNumber)
	{
		waitForPlayersCv.wait(ulock);
	}
	
	Question *question = m_questions[m_currentQuestionNumber];
	return question;
}

void Game::submitAnswer(LoggedUser user, float answerTimeout, int answerId)
{
	if (getQuestion(user)->getCorrectAnswer() == answerId)
	{
		m_players[user.getUsername()].correctAnswerCount++;
	}
	else
	{
		m_players[user.getUsername()].wrongAnswerCount++;
	}
	if (++m_currentQuestionSubmits == m_players.size())
	{
		m_currentQuestionSubmits = 0;
		if (++m_currentQuestionNumber == m_questions.size())
		{
			setGameResults();
			m_gameOver = true;
			waitForGameToEndCv.notify_all();
		}
		waitForPlayersCv.notify_all();
	}
	m_players[user.getUsername()].averageAnswerTime += answerTimeout / m_questions.size();
}

int Game::removePlayer(LoggedUser user)
{
	if (m_currentQuestionSubmits == m_players.size() - 1)
	{
		m_currentQuestionSubmits = 0;
		if (++m_currentQuestionNumber == m_questions.size())
		{
			setGameResults();
			m_gameOver = true;
			waitForGameToEndCv.notify_all();
		}
		else
		{
			m_results.push_back(m_players[user.getUsername()]);
			waitForPlayersCv.notify_all();
		}
	}
	else
	{
		m_results.push_back(m_players[user.getUsername()]);
	}

	m_players.erase(user.getUsername());
	return m_players.size();
}

void Game::setGameResults()
{
	for (pair<string, PlayerResults> playerResult : m_players)
	{
		m_results.push_back(playerResult.second);
	}
	std::sort(m_results.begin(), m_results.end());
}

int Game::getCorrectAnswer(LoggedUser user)
{
	return getQuestion(user)->getCorrectAnswer();
}

bool Game::isOver()
{
	return m_gameOver;
}
bool Game::isPlayerFinished(LoggedUser user)
{
	return m_questions.size() == getCurrentQuestionNumber(user);
}

int Game::getGameId()
{
	return m_gameId;
}

vector<PlayerResults> Game::getResults()
{
	if (!m_gameOver)
	{
		std::unique_lock<mutex> ulock(waitForGameToEndMtx);
		waitForGameToEndCv.wait(ulock);
	}
	return m_results;
}

int Game::getCurrentQuestionNumber(LoggedUser user)
{
	return m_players[user.getUsername()].correctAnswerCount + m_players[user.getUsername()].wrongAnswerCount;
}
