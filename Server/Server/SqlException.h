#pragma once
#include <exception>

class SqlException : public std::exception
{
public:
	virtual const char* what() const throw();
};