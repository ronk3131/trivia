#include "JsonRequestPacketDeserializer.h"
#include "json.hpp"

using json = nlohmann::json;
using std::string;

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(char *buffer)
{
	json j = json::parse(buffer);

	LoginRequest request;
	request.username = j["username"].get<string>();
	request.password = j["password"].get<string>();

	return request;
}

SignUpRequest JsonRequestPacketDeserializer::deserializeSignUpRequest(char *buffer)
{
	json j = json::parse(buffer);

	SignUpRequest request;
	request.email = j["email"].get<string>();
	request.username = j["username"].get<string>();
	request.password = j["password"].get<string>();

	return request;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(char *buffer)
{
	json j = json::parse(buffer);

	GetPlayersInRoomRequest request;
	request.roomId = j["roomId"].get<unsigned int>();
	
	return request;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(char *buffer)
{
	json j = json::parse(buffer);

	JoinRoomRequest request;
	request.roomId = j["roomId"].get<unsigned int>();

	return request;
}

KickPlayerRequest JsonRequestPacketDeserializer::deserializeKickPlayerRequest(char *buffer)
{
	json j = json::parse(buffer);

	KickPlayerRequest request;
	request.playerName = j["playerName"].get<string>();

	return request;
}

SendMessageRequest JsonRequestPacketDeserializer::deserializeSendMessageRequest(char *buffer)
{
	json j = json::parse(buffer);

	SendMessageRequest request;
	request.message = j["message"].get<string>();

	return request;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(char *buffer)
{
	json j = json::parse(buffer);

	CreateRoomRequest request;
	request.roomName = j["roomName"].get<string>();
	request.answerTimeout = j["answerTimeout"].get<float>();
	request.maxUsers = j["maxUsers"].get<unsigned int>();
	request.questionCount = j["questionCount"].get<unsigned int>();
	
	return request;
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(char *buffer)
{
	json j = json::parse(buffer);

	SubmitAnswerRequest request;
	request.answerId = j["answerId"];
	
	return request;
}
