#pragma once
#include <iostream>
#include "IDataBase.h"
#include "sqlite3.h"
#include "sql_callbacks.h"
#include "SqlException.h"
#include <io.h>
#include "Response.h"
#include <sstream>

using std::stringstream;

class Sqlite : public IDataBase
{
public:
	~Sqlite();

	static Sqlite *getObject();

	map<string, int> getHighScores() override;
	bool doesUsernameTaken(string) override;
	bool doesEmailTaken(string) override;
	void addUser(string, string, string) override;
	bool login(string, string) override;
	int getGameCountOfPlayer(string) override;
	int getRightAnswerCountOfPlayer(string) override;
	int getWrongAnswersCountOfPlayer(string) override;
	float getAvergaeTimeForAnswerOfPlayer(string) override;
	vector<Question*> getQuestions(int) override;
	int getQuestionsCount() override;
	void updatePlayersResults(vector<PlayerResults>);

private:
	Sqlite();

	template<class T>
	T execute(string);
	void execute(string);
	bool isProtectedFromSqlInjection(vector<string>);
	static Sqlite *m_object; // initialized in cpp file.

	static const string ILLEGAL_CHARS;
	
	sqlite3 *m_db;
};