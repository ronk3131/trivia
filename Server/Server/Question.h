#pragma once
#include <string>
#include <vector>

using std::string;
using std::vector;

class Question
{
public:
	Question(string question, string answer0, string answer1, string answer2, string answer3, int correctAnswer);
	Question();

	//getters
	string getQuestion();
	vector<string> getPossibleAnswers();
	int getCorrectAnswer();

	//setters
	void setQuestion(string question);
	void setPossibleAnswer(string answer);
	void setCorrectAnswer(int correctAnswer);
	void clearPossibleAnswers();

private:
	string m_question;
	vector<string> m_possibleAnswers;
	int m_correctAnswer;
};