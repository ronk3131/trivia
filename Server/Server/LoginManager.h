#pragma once
#include <vector>
#include "IDataBase.h" //This header contains LoggedUser.h
#include "ResponseCode.h"
#include "SqlException.h"

using std::vector;
using std::string;

class LoginManager
{
public:
	static LoginManager *createObject(IDataBase*);
	static LoginManager *getObject();
	
	ResponseCode signup(string, string, string);
	ResponseCode login(string, string);
	bool isLoggedIn(string);
	void logout(string);

	static LoginManager *m_object;

	static const int USER_MIN_LEN = 3;
	static const int PASSWORD_MIN_LEN = 6;

private:
	LoginManager(IDataBase*);

	IDataBase *m_database;
	vector<LoggedUser> m_loggedUsers;
	static bool isValidUsername(string);
	static bool isValidPassword(string);
	static bool isValidEmail(string);
};
