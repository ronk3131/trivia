#include "RoomRequestHandler.h"

RoomAdminRequestHandler::RoomAdminRequestHandler(RequestHandlerFactory *handlerFactory, Room *room, LoggedUser user, LoginManager *loginManager, RoomManager *roomManager) : RoomMemberRequestHandler(handlerFactory, room, user, loginManager, roomManager)
{
}

bool RoomAdminRequestHandler::isRequestRelevant(Request request)
{
	switch (request.id)
	{
		case RequestCode::StartGame: return true;
		case RequestCode::KickPlayer: return request.buffer != NULL;
		default: return RoomMemberRequestHandler::isRequestRelevant(request);
	};
}

RequestResult *RoomAdminRequestHandler::handleRequest(Request request)
{
	switch (request.id)
	{
		case RequestCode::StartGame: return startGame(request);
		case RequestCode::KickPlayer: return kickPlayer(request);
		default: return RoomMemberRequestHandler::handleRequest(request);
	}
}

void RoomAdminRequestHandler::closeRoom()
{
	m_roomManager->deleteRoom(m_room->getMetadata().id);
}

RequestResult *RoomAdminRequestHandler::leaveRoom(Request request)
{
	delete RoomMemberRequestHandler::leaveRoom(request);

	RoomClosedRequestUpdate *result = new RoomClosedRequestUpdate;

	result->updateRoomType = ResponseCode::RoomClosed;
	result->users = m_room->getAllUsers();
	result->sockets = m_room->getAllSockets();
	result->response = NULL;
	result->newHandler = m_handlerFactory->createMenuRequestHandler(m_user);
	
	closeRoom();
	
	return result;
}

RequestResult *RoomAdminRequestHandler::kickPlayer(Request request)
{
	RequestResult *result = new RequestResult;
	KickPlayerResponse kickRes;

	try
	{
		KickPlayerRequest kickReq = JsonRequestPacketDeserializer::deserializeKickPlayerRequest(request.buffer);

		if (!m_room->hasUser(LoggedUser(kickReq.playerName)))
		{
			kickRes.status = ResponseCode::UserNotFound;
		}
		else if (m_user.getUsername() == kickReq.playerName)
		{
			kickRes.status = ResponseCode::CantKickAdmin;
		}
		else
		{
			delete result;
			result = new KickPlayerRequestUpdate;
			KickPlayerRequestUpdate *kickUpdate = (KickPlayerRequestUpdate*)result;
			
			// update the players in room (including admin).
			kickUpdate->updateRoomType = ResponseCode::RoomKickPlayer;
			kickUpdate->playerName = kickReq.playerName;
			kickUpdate->playerSocket = m_room->getSocket(kickReq.playerName);
			kickUpdate->sockets = m_room->getAllSockets();

			// kick the player.
			m_room->removeUser(LoggedUser(kickReq.playerName));
			kickRes.status = ResponseCode::Success;
		}
	}
	catch(std::exception)
	{
		kickRes.status = ResponseCode::UserNotFound;
	}

	if (kickRes.status != ResponseCode::Success)
	{
		result->response = Communicator::createResponse(kickRes.status, JsonResponsePacketSerializer::serializeResponse(kickRes));
	}
	else
	{
		result->response = Communicator::createResponse(kickRes.status); // the communicator will update him with the rest of the players in the room.
	}
	result->newHandler = this;

	return result;
}

RequestResult *RoomAdminRequestHandler::startGame(Request request)
{
	StartGameRequestUpdate *result = new StartGameRequestUpdate;

	result->newHandler = m_handlerFactory->createGameRequestHandler(m_user, m_room);

	m_room->removeUser(m_user); // prevent updating admin.
	result->response = NULL;
	result->sockets = m_room->getAllSockets();
	result->users = m_room->getAllUsers();
	result->gameId = m_room->getMetadata().id;
	result->updateRoomType = ResponseCode::GameStarted;

	m_room->startGame();
	closeRoom();

	return result;
}
