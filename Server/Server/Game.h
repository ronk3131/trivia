#pragma once
#include <vector>
#include <map>
#include "Question.h"
#include "LoggedUser.h"
#include "Response.h"
#include "PlayerResults.h"
#include <mutex>
#include <condition_variable>

using std::vector;
using std::map;
using std::mutex;
using std::condition_variable;

class Game
{
public:
	Game(int, vector<Question*>, vector<LoggedUser>);
	Question *getQuestion(LoggedUser);
	void submitAnswer(LoggedUser, float, int);
	int removePlayer(LoggedUser); // return how many players left.
	int getCorrectAnswer(LoggedUser);
	vector<PlayerResults> getResults();
	bool isOver();
	bool isPlayerFinished(LoggedUser);
	int getGameId();

private:
	int getCurrentQuestionNumber(LoggedUser);
	void setGameResults();

	mutex waitForPlayersMtx;
	condition_variable waitForPlayersCv;
	
	mutex waitForGameToEndMtx;
	condition_variable waitForGameToEndCv;

	int m_gameId;
	bool m_gameOver;
	int m_currentQuestionNumber;
	int m_currentQuestionSubmits;
	vector<Question*> m_questions;
	map<string, PlayerResults> m_players;
	vector<PlayerResults> m_results;
};