import socket
import json

HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 8820        # Port to listen on (non-privileged ports are > 1023)

run = 1

def createRequest(code, serialized):
    return str(code).zfill(3) + str(len(serialized)).zfill(4) + serialized
def printResponse(response):
    print("Response Code:",int(response[0:3]))
    print("Data Length:",int(response[3:7]))
    responseJson = json.loads(response[7::])
    temp = "password and username does not match."
    if(responseJson["message"] != temp):
        print ("Error")
        run = 0
    else:
        print("Actual Message:",responseJson["message"])

    print();
    

loginCode = 2
loginMessage =  {"email":"ron123@gmail.com", "username":"","password":""}
loginSerialized = json.dumps(loginMessage)

createCode = 7
createMessage =  {"roomId":0, "roomName":"Room2","maxUsers":2 , "questionCount":2, "answerTimeout":2.0}
createSerialized = json.dumps(createMessage)

leaveCode = 12
leaveMessage =  {"roomId":0, "roomName":"Room2","maxUsers":2 , "questionCount":2, "answerTimeout":2.0}
leaveSerialized = json.dumps(leaveMessage)


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))

    while run:
        request = createRequest(loginCode, loginSerialized)
        s.send(request.encode())
        printResponse(s.recv(1024).decode())

    request = createRequest(createCode, createSerialized)
    s.send(request.encode())
    printResponse(s.recv(1024).decode())

    s.recv(1024)
    
    request = createRequest(leaveCode, leaveSerialized)
    s.send(request.encode())
    printResponse(s.recv(1024).decode())
    while(True):
        print(s.recv(1024).decode())

