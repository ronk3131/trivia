﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for GameResults.xaml
    /// </summary>
    public partial class GameResults : Window
    {
        public GameResults(GetGameResultsResult gameResults)
        {
            InitializeComponent();
            Closing += App.Window_Closing;
            Closing += delegate { App.GetClient().LeaveGame(); };
            backToMainButton.Click += delegate { App.BackToMain(); };
            usernameLabel.Content = App.GetClient().GetUsername();


            Label usersLabel = CreateNewPlayerResultLabel();
            for (int i = 0; i < gameResults.playerResults.Count(); i++)
            {
                usersLabel.Content += String.Format("username: {0}, score: {1}{2}", gameResults.playerResults[i].username, gameResults.playerResults[i].score, Environment.NewLine);
            }
            scrollPanel.Children.Add(usersLabel);
        }
        public Label CreateNewPlayerResultLabel()
        {
            Label label = new Label();
            label.FontSize = 24;
            label.Width = double.NaN;
            label.Height = double.NaN;
            label.HorizontalAlignment = HorizontalAlignment.Center;
            label.HorizontalContentAlignment = HorizontalAlignment.Center;
            label.FontFamily = new FontFamily("Arial");
            label.FontWeight = FontWeights.Bold;
            return label;
        }
    }
}