﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for BestScores.xaml
    /// </summary>
    public partial class BestScores : Window
    {
        public BestScores()
        {
            InitializeComponent();
            Closing += App.Window_Closing;
            backButton.Click += delegate { App.BackToMain(); };

            BestScoresResult bestScores = App.GetClient().BestScores();
            bestScores.users.Sort((x, y) => y.Item2.CompareTo(x.Item2));

            if (bestScores.users != null)
            {
                Label usersLabel = CreateNewBestScoreLabel();
                foreach (Tuple<string, int> user in bestScores.users)
                {
                    usersLabel.Content += String.Format("{0}: {1}{2}", user.Item1, user.Item2, Environment.NewLine);
                }
                scrollPanel.Children.Add(usersLabel);
            }
            else
            {
                Label errorLabel = CreateNewBestScoreLabel();
                errorLabel.Content = bestScores.errorMessage ?? "something bad happened to the server";
                scrollPanel.Children.Add(errorLabel);
            }
        }
        public Label CreateNewBestScoreLabel()
        {
            Label label = new Label();
            label.FontSize = 24;
            label.Width = double.NaN;
            label.Height = double.NaN;
            label.HorizontalAlignment = HorizontalAlignment.Center;
            label.HorizontalContentAlignment = HorizontalAlignment.Center;
            label.Margin = new Thickness(70,0,0,0);
            label.FontFamily = new FontFamily("Arial");
            label.FontWeight = FontWeights.Bold;
            return label;
        }
    }
}
