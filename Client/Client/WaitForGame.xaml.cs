﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Documents;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for WaitForGame.xaml
    /// </summary>
    public partial class WaitForGame : Window
    {
        public WaitForGame(bool isAdmin)
        {
            InitializeComponent();

            usernameLabel.Content = App.GetClient().GetUsername();
            CreateChatMessage("You have joined the room.");

            this.isAdmin = isAdmin;
            if (isAdmin)
            {
                leaveRoomButton.Visibility = Visibility.Collapsed;
            }
            else
            {
                kickPlayerButton.Visibility = Visibility.Collapsed;
                closeRoomButton.Visibility = Visibility.Collapsed;
                startGameButton.Visibility = Visibility.Collapsed;
            }

            Closing += App.Window_Closing;

            ShowRoomInfo();

            updateRoomTask = Task.Run(() => UpdateRoom());
        }

        private void UpdateRoom()
        {
            isUpdateRoomCancelled = false;

            while (!isUpdateRoomCancelled)
            {
                RoomUpdateResult roomUpdateResult;
                try { roomUpdateResult = App.GetClient().GetRoomUpdate(ref isUpdateRoomCancelled, ref isUpdateRoomBlocked); }
                catch { return; }

                switch(roomUpdateResult.updateType)
                {
                    case UpdateRoomType.CloseRoom:
                        {
                            Task.Run(() => Dispatcher.Invoke(() => { ChangeWindow(false, true); }));
                            break;
                        }
                    case UpdateRoomType.StartGame:
                        {
                            Task.Run(() => Dispatcher.Invoke(() => { ChangeWindow(true, false); }));
                            break;
                        }
                    case UpdateRoomType.KickPlayer:
                        {
                            if (App.GetClient().GetUsername() == roomUpdateResult.playerName)
                            {
                                Dispatcher.Invoke(() => MessageBox.Show(this, "You have been kicked.", "Notification", MessageBoxButton.OK, MessageBoxImage.Information));
                                Task.Run(() => Dispatcher.Invoke(() => { ChangeWindow(false, true); }));
                            }
                            else
                            {
                                Dispatcher.Invoke(() =>
                                {
                                    scrollPanelParticipants.Children.Remove(playerLabels[roomUpdateResult.playerName]);
                                    playerLabels.Remove(roomUpdateResult.playerName);
                                    CreateChatMessage(roomUpdateResult.playerName + " has been kicked.");
                                });
                            }
                            break;
                        }
                    case UpdateRoomType.LeaveJoin:
                        {
                            if (playerLabels.ContainsKey(roomUpdateResult.playerName))
                            {
                                Dispatcher.Invoke(() =>
                                {
                                    scrollPanelParticipants.Children.Remove(playerLabels[roomUpdateResult.playerName]);
                                    playerLabels.Remove(roomUpdateResult.playerName);
                                    CreateChatMessage(roomUpdateResult.playerName + " has left the room.");
                                });
                            }
                            else
                            {
                                Dispatcher.Invoke(() =>
                                {
                                    Label playerLabel = CreateNewPlayerLabel(roomUpdateResult.playerName);
                                    playerLabels[roomUpdateResult.playerName] = playerLabel;
                                    scrollPanelParticipants.Children.Add(playerLabel);
                                    CreateChatMessage(roomUpdateResult.playerName + " has joined the room.");
                                });
                            }
                            break;
                        }
                    case UpdateRoomType.NewMessage:
                        {
                            Dispatcher.Invoke(() => CreateChatMessage(roomUpdateResult.message, roomUpdateResult.playerName));
                            break;
                        }
                }
            }
        }

        private Label CreateNewPlayerLabel(string playerName)
        {
            Label label = new Label
            {
                FontSize = 16,
                Width = scrollViewerParticipants.Width,
                Height = double.NaN,
                HorizontalAlignment = HorizontalAlignment.Center,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                FontFamily = new FontFamily("Arial"),
                FontWeight = FontWeights.Bold,
                Content = playerName
            };
            if(isAdmin)
            {
                label.MouseLeftButtonUp += delegate { ChoosePlayerToKick(playerName); };
            }
            return label;
        }
        private void CreateChatMessage(string message, string playerName = null)
        {
            if(playerName == null)
            {
                playerName = "System";
            }

            TextBlock textBlock = new TextBlock
            {
                FontSize = 22,
                Width = 500,
                TextWrapping = TextWrapping.Wrap,
                Margin = new Thickness(10)
            };
            textBlock.Inlines.Add(new Bold(new Underline(new Italic(new Run(playerName  + ":")))));
            textBlock.Inlines.Add(" " + message);

            chatPanel.Children.Add(textBlock);
        }

        private void ShowRoomInfo()
        {
            // this should be get room status
            GetRoomStateResult getRoomStateResult = App.GetClient().GetRoomState();

            if (getRoomStateResult.success)
            {
                playerLabels = new Dictionary<string, Label>();
                foreach (string playerName in getRoomStateResult.room.players)
                {
                    Label playerLabel = CreateNewPlayerLabel(playerName);
                    playerLabels[playerName] = playerLabel;
                    scrollPanelParticipants.Children.Add(playerLabel);
                }
                max_number_players.Content = "max number of players: " + getRoomStateResult.room.maxPlayersCount;
                number_of_questions.Content = "number of questions: " + getRoomStateResult.room.questionCount;
                time_per_question.Content = "time per question: " + getRoomStateResult.room.answerTimeout;
                room_title.Content = "Room: " + getRoomStateResult.room.roomName;
            }
            else
            {
                errorMessage.Text = getRoomStateResult.errorMessage;
            }
        }

        public void LeaveRoomOnClick(object o, EventArgs e)
        {
            isUpdateRoomBlocked = true;
            App.GetClient().LeaveRoom();
            ChangeWindow(false, true);
            isUpdateRoomBlocked = false;
        }

        public void StartGameOnClick(object o, EventArgs e)
        {
            isUpdateRoomBlocked = true;
            App.GetClient().StartGame();
            ChangeWindow(true, false);
            isUpdateRoomBlocked = false;
        }
   
        private void ChoosePlayerToKick(string playerName)
        {
            if (playerToKick != null)
            {
                playerLabels[playerToKick].Background = Brushes.White;
                errorMessage.Text = "";
            }
            playerToKick = playerName;
            playerLabels[playerToKick].Background = Brushes.Blue;
        }
        private void KickPlayerOnClick(object o, EventArgs e)
        {
            isUpdateRoomBlocked = true;
            KickPlayerResult res = App.GetClient().KickPlayer(playerToKick);
            if(res.success)
            {
                playerToKick = null;
            }
            else
            {
                errorMessage.Text = res.errorMessage;
            }
            isUpdateRoomBlocked = false;
        }
        private void SendButtonOnClick(object o, EventArgs e)
        {
            if(messageTextBox.Text != null)
            {
                App.GetClient().SendMessage(messageTextBox.Text);
                messageTextBox.Text = "";
            }
        }
        private void ChangeWindow(bool isStart, bool isLeave)
        {
            isUpdateRoomCancelled = true;
            while (!updateRoomTask.IsCompleted)
            {
                // wait...
            }

            if (isStart) App.ShowWindow(new Game(App.GetClient().GetCurrentRoom()));
            else if (isLeave) App.ShowMainWindow();

            App.closeAllWindows = false;
            this.Close();
        }
        private Task updateRoomTask;
        private bool isUpdateRoomCancelled;
        private bool isUpdateRoomBlocked;
        private bool isAdmin;

        private Dictionary<string, Label> playerLabels;
        private string playerToKick;
    }
}
