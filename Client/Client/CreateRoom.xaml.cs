﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for CreateRoom.xaml
    /// </summary>
    public partial class CreateRoom : Window
    {
        public CreateRoom()
        {
            InitializeComponent();
            backButton.Click += delegate { App.BackToMain(); };
            this.Closing += App.Window_Closing;
        }
        private void CreateRoomOnClick(object o, EventArgs e)
        {
            try
            {
                CreateRoomResult result = App.GetClient().CreateRoom(inputRoomName.Text, int.Parse(inputPlayersCount.Text), int.Parse(inputQuestionsCount.Text), double.Parse(inputTimeForQuestion.Text));
                if (result.success)
                {
                    App.closeAllWindows = false;
                    App.ShowWindow(new WaitForGame(true));
                    this.Close();
                }
                else
                {
                    errorMessage.Text = result.errorMessage;
                }
            }
            catch
            {
                /* Possible errors:
                 * user types in playersCount/questionCount a non integer text.
                 * user types in timeForAnswer a non double text.
                 */
                errorMessage.Text = "Invalid Inputs";
            } 
        }
    }
}
