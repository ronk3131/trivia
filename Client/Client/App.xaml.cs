﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Reflection;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        void Application_Startup(object sender, StartupEventArgs e)
        {
            closeAllWindows = true;
            currentWindow = null;

            bool exit = false;
            while (!exit)
            {
                try
                {
                    Tuple<string, int> serverInfo = GetServerInfo(CONFIG_PATH);
                    _client = new Client(serverInfo.Item1, serverInfo.Item2);
                    if (_client.IsConnected())
                    {
                        // Create main application window, starting minimized if specified
                        _mainWindow = new MainWindow();
                        ShowWindow(_mainWindow);
                    }
                    exit = true;
                }
                catch (Exception)
                {
                    MessageBoxResult result = MessageBox.Show("The server is not running OR the config file is invalid\n\nOk - try again\nCancel - close client", "Error", MessageBoxButton.OKCancel, MessageBoxImage.Error);
                    exit = result == MessageBoxResult.Cancel;
                    if(exit)
                    {
                        //close client application
                        Application.Current.Shutdown();
                    }
                }
            }
        }
        
        public static MyWindow ShowWindow<MyWindow>() where MyWindow : Window, new()
        {
            MyWindow nextWindow = new MyWindow();
            ShowWindow(nextWindow);
            return nextWindow;
        }

        public static void ShowWindow(Window nextWindow)
        {
            if (currentWindow != null)
            {
                nextWindow.Left = currentWindow.Left;
                nextWindow.Top = currentWindow.Top;
                currentWindow.Hide();
            }

            nextWindow.Show();
            
            currentWindow = nextWindow;
        }

        public static Client GetClient()
        {
            return _client;
        }
        
        public static void ShowMainWindow()
        {
            ShowWindow(_mainWindow);
        }

        public static void CloseMainWindow()
        {
            _mainWindow.Close();
        }
        public static MainWindow GetMainWindow()
        {
            return _mainWindow;
        }
        
        public static void Window_Closing(object o, EventArgs e)
        {
            if (closeAllWindows)
            {
                CloseMainWindow();
            }
            closeAllWindows = true;
        }

        public static void BackToMain()
        {
            closeAllWindows = false;
            currentWindow.Close();
            ShowWindow(_mainWindow);
        }

        private Tuple<string, int> GetServerInfo(string file)
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), file);
            string[] serverInfoParts = System.IO.File.ReadAllText(path).Replace(" ", string.Empty).Replace("\r", string.Empty).Split('\n');

            string ip = "";
            int port = 0;

            for (int i = 0; i < serverInfoParts.Length; i++)
            {
                if (serverInfoParts[i].StartsWith(CONFIG_SERVER_IP_FORMAT))
                    ip = serverInfoParts[i].Substring(CONFIG_SERVER_IP_FORMAT.Length);

                else if (serverInfoParts[i].StartsWith(CONFIG_SERVER_PORT_FORMAT))
                    port = int.Parse(serverInfoParts[i].Substring(CONFIG_SERVER_PORT_FORMAT.Length));
            }
            return new Tuple<string, int>(ip, port);
        }
        public static bool closeAllWindows;
        private static Client _client;
        private static MainWindow _mainWindow;
        private static Window currentWindow;

        private static readonly string CONFIG_PATH = "config.txt";
        private static readonly string CONFIG_SERVER_IP_FORMAT = "server_ip=";
        private static readonly string CONFIG_SERVER_PORT_FORMAT = "port=";
    }
}
