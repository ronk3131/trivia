﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Threading;
using System.ComponentModel;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for Game.xaml
    /// </summary>
    public partial class Game : Window
    {
        public Game(RoomMetaData room)
        {
            InitializeComponent();
            Closing += App.Window_Closing;

            usernameLabel.Content = App.GetClient().GetUsername();
            roomNameLabel.Content = room.roomName;

            answerTimeleft = room.answerTimeout;
            questionsLeft = room.questionCount;

            answerButtons = new Label[answersGroup.Children.Count];
            for(int i = 0; i < answerButtons.Length; i++)
            {
                int answerId = i; // dont touch this.
                answerButtons[i] = (Label)answersGroup.Children[i];
                answerButtons[i].MouseLeftButtonUp += delegate { SubmitAnswer(answerId); };
            }

            timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Background, delegate
            {
                Dispatcher.Invoke(() => timeleftLabel.Content = "Time left: " + questionTimeleft);
                if (questionTimeleft == 0)
                {
                    SubmitAnswer(NO_ANSWER_ID);
                }
                questionTimeleft--;
            }, Application.Current.Dispatcher);

            nextQuestionWorker = new BackgroundWorker();
            nextQuestionWorker.DoWork += delegate { NextQuestion(); };

            getGameResultsWorker = new BackgroundWorker();
            getGameResultsWorker.DoWork += delegate
            {
                GetGameResultsResult gameResults = App.GetClient().GetGameResults();
                App.closeAllWindows = false;
                Dispatcher.Invoke(delegate
                {
                    App.ShowWindow(new GameResults(gameResults));
                    this.Close();
                });
            };

            Task.Run(() => StartGame());
        }

        private void StartGame()
        {
            while (questionsLeft > 0)
            {
                nextQuestionWorker.RunWorkerAsync();
                WaitForPlayers(nextQuestionWorker);
                
                questionTimeleft = (int)answerTimeleft;
                timer.Start();

                isQuestionSubmitted = false;
                while(!isQuestionSubmitted && timer.IsEnabled)
                {
                    // wait for submit or timer elapsed.
                }

                Thread.Sleep(QUESTION_DELAY);
            }
            
            getGameResultsWorker.RunWorkerAsync();
            WaitForPlayers(getGameResultsWorker);
        }

        private void WaitForPlayers(BackgroundWorker worker)
        {
            Thread.Sleep(LOADING_SCREEN_DELAY);
            if (worker.IsBusy)
            {
                Dispatcher.Invoke(() => waitingProgressBar.Visibility = Visibility.Visible);
            }
            while (worker.IsBusy)
            {
                // wait...
            }
            Dispatcher.Invoke(() => waitingProgressBar.Visibility = Visibility.Collapsed);
        }

        private void SubmitAnswer(int answerId)
        {
            if (!isQuestionSubmitted)
            {
                SubmitAnswerResult submitAnswerRes = App.GetClient().SubmitAnswer(answerId);
                if (submitAnswerRes.success)
                {
                    isQuestionSubmitted = true;
                    foreach (Label answerButton in answerButtons)
                    {
                        answerButton.IsEnabled = false;
                        if (answerId == NO_ANSWER_ID)
                        {
                            answerButton.Background = WRONG_ANSWER_COLOR;
                        }
                    }
                    answerButtons[submitAnswerRes.correctAnswer].Background = CORRECT_ANSWER_COLOR;
                    if (answerId != NO_ANSWER_ID && answerId != submitAnswerRes.correctAnswer)
                    {
                        answerButtons[answerId].Background = WRONG_ANSWER_COLOR;
                    }
                    questionsLeft--;
                    timer.Stop();
                }
            }
        }

        private void NextQuestion()
        {
            //get question result from the server
            GetQuestionResult questionRes;
            
            do
            {
                questionRes = App.GetClient().GetNextQuestion();
            } while (!questionRes.success);
            
            if (questionRes.success)
            {
                Dispatcher.Invoke(delegate
                {
                    //init questionsLeft
                    questionsLeftLabel.Content = "questions left: " + Convert.ToString(questionsLeft);
                    timeleftLabel.Content = "Time left: " + Convert.ToString(answerTimeleft);

                    //init question;
                    questionTextBlock.Text = questionRes.question.question;

                    //init answers
                    for (int i = 0; i < answerButtons.Length; ++i)
                    {
                        answerButtons[i].IsEnabled = true;
                        answerButtons[i].Content = questionRes.question.answers[i];
                        answerButtons[i].Background = DEFAULT_ANSWER_COLOR;
                    }
                });
            }
        }

        private const int NO_ANSWER_ID = -1;
        private const int QUESTION_DELAY = 1000;
        private const int LOADING_SCREEN_DELAY = 100;
        private SolidColorBrush CORRECT_ANSWER_COLOR = Brushes.Green;
        private SolidColorBrush WRONG_ANSWER_COLOR = Brushes.Red;
        private SolidColorBrush DEFAULT_ANSWER_COLOR = Brushes.LightGray;

        private BackgroundWorker nextQuestionWorker;
        private BackgroundWorker getGameResultsWorker;

        private DispatcherTimer timer;
        private int questionTimeleft;
        private Label[] answerButtons;
        private int questionsLeft;
        private double answerTimeleft;
        private bool isQuestionSubmitted;
    }
}
