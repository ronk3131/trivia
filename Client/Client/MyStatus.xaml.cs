﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for MyStatus.xaml
    /// </summary>
    public partial class MyStatus : Window
    {
        private const string PerformanceValueFormat = "{[value]}";

        public MyStatus()
        {
            InitializeComponent();
            backButton.Click += delegate { App.BackToMain(); };
            this.Closing += App.Window_Closing;

            PerformancesResult performancesResult = App.GetClient().GetPerformances();
            if (performancesResult.success)
            {
                SetPerformances(performancesResult);
            }
            else
            {
                MessageBoxResult result = MessageBox.Show(performancesResult.errorMessage, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                App.CloseMainWindow();
                this.Close();
            }
        }
        private void SetPerformances(PerformancesResult performances)
        {
            SetPerformance(LabelGamesCount, performances.gamesCount.ToString());
            SetPerformance(LabelRightAnswersCount, performances.rightAnswersCount.ToString());
            SetPerformance(LabelWrongAnswersCount, performances.wrongAnswersCount.ToString());
            SetPerformance(LabelAverageTime, performances.averageTimeForAnswer.ToString("F3"));
        }
        private void SetPerformance(Label label, string value)
        {
            label.Content = label.Content.ToString().Replace(PerformanceValueFormat, value);
        }
    }
}
