﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace client
{
    [Activity(Label = "JoinRoomWIndow")]
    public class JoinRoomWIndow : Activity
    {
        int chosenRoomID;
        Dictionary<int, TextView> rooms;
        LinearLayout roomsPanel, playersPanel,playersContainer;
        TextView errorMessage;
        Button joinRoom, refresh, back;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_joinRoom);

            errorMessage = FindViewById<TextView>(Resource.Id.joinRoom_errorMessage);
            roomsPanel = FindViewById<LinearLayout>(Resource.Id.joinRoom_roomsPanel);
            joinRoom = FindViewById<Button>(Resource.Id.joinRoom_join);
            refresh = FindViewById<Button>(Resource.Id.joinRoom_refresh);
            playersContainer = FindViewById<LinearLayout>(Resource.Id.joinRoom_playersContainer);
            playersPanel = FindViewById<LinearLayout>(Resource.Id.joinRoom_playersPanel);
            back = FindViewById<Button>(Resource.Id.joinRoom_back);

            chosenRoomID = -1;
            ShowRoomList();
            joinRoom.Click += JoinRoomOnClick;
            refresh.Click += RefreshRoomsOnClick;
            back.Click += delegate { LoadingWindow.ShowWindow<MainWindow>(this, true); };
        }
        private TextView CreateNewRoomTextView(string content)
        {
            TextView room = new TextView(this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);
            layoutParams.SetMargins(0, 5, 0, 0);
            room.LayoutParameters = layoutParams;
            room.SetBackgroundColor(Color.White);
            room.Gravity = GravityFlags.CenterVertical;
            room.TextSize = 20;
            room.Text = content;
            room.SetPadding(10, 10, 0, 10);
            return room;
        }

        private TextView CreateNewPlayerTextView(string content)
        {
            TextView player = new TextView(this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);
            layoutParams.SetMargins(0, 5, 0, 0);
            player.LayoutParameters = layoutParams;
            player.SetBackgroundColor(Color.White);
            player.Gravity = GravityFlags.CenterVertical;
            player.TextSize = 20;
            player.Text = content;
            player.SetPadding(10, 10, 0, 10);
            return player;
        }

        private void ShowRoomList()
        {
            GetRoomsResult getRoomsResult = Client.GetClient().GetRooms();
            if (getRoomsResult.success)
            {
                rooms = new Dictionary<int, TextView>();
                foreach (KeyValuePair<int, string> roomData in getRoomsResult.rooms)
                {
                    TextView room = CreateNewRoomTextView(roomData.Value);
                    int roomId = roomData.Key; // must do this for the delegate.
                    room.Click += delegate { ShowRoomPlayers(roomId); };
                    rooms[roomId] = room;
                    roomsPanel.AddView(room);
                }
            }
        }

        private void ClearRoomList()
        {
            roomsPanel.RemoveAllViews();
        }
        private void ClearPlayersList()
        {
            playersContainer.Visibility = ViewStates.Gone;
            playersPanel.RemoveAllViews();
            chosenRoomID = -1;
        }

        private void RefreshRoomsOnClick(object o, EventArgs e)
        {
            ClearRoomList();
            ClearPlayersList();
            ShowRoomList();
        }

        private void JoinRoomOnClick(object o, EventArgs e)
        {
            string roomName = null;
            if (rooms.ContainsKey(chosenRoomID))
            {
                roomName = Convert.ToString(rooms[chosenRoomID].Text);
            }

            JoinRoomResult joinRoomResult = Client.GetClient().JoinRoom(chosenRoomID, roomName);
            if (joinRoomResult.success)
            {
                LoadingWindow.ShowWindow<RoomWindow>(this, true);
            }
            else
            {
                LoadingWindow.ShowAlert(this, joinRoomResult.errorMessage);
            }
        }

        
        private void ShowRoomPlayers(int roomId)
        {
            if (chosenRoomID != -1)
            {
                rooms[chosenRoomID].SetBackgroundColor(Color.White);
                ClearPlayersList();
            }
            
            chosenRoomID = roomId;
            rooms[chosenRoomID].SetBackgroundColor(Color.Blue);

            GetPlayersInRoomResult getPlayersResult = Client.GetClient().GetPlayersInRoom(chosenRoomID);

            if (getPlayersResult.success)
            {
                playersContainer.Visibility = ViewStates.Visible;
                foreach (string playerName in getPlayersResult.players)
                {
                    TextView player = CreateNewPlayerTextView(playerName);
                    playersPanel.AddView(player);
                }
            }
            else
            {
                LoadingWindow.ShowAlert(this, getPlayersResult.errorMessage);
            }
        }
    }
}