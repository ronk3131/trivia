﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;

namespace client
{
    [Activity(Label = "SignUpWindow")]
    public class SignUpWindow : Activity
    {
        TextView errorMessage;
        EditText inputEmail, inputUser, inputPass;
        Button signUp, gotoSignIn;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_signup);
            errorMessage = FindViewById<TextView>(Resource.Id.signup_errorMessage);
            inputEmail = FindViewById<EditText>(Resource.Id.signup_inputEmail);
            inputUser = FindViewById<EditText>(Resource.Id.signup_inputUser);
            inputPass = FindViewById<EditText>(Resource.Id.signup_inputPass);
            signUp = FindViewById<Button>(Resource.Id.signup_signUp);
            gotoSignIn = FindViewById<Button>(Resource.Id.signup_gotoSignIn);
            
            signUp.Click += SignUp;
            gotoSignIn.Click += GotoSignIn;
        }
        private void SignUp(object o, EventArgs e)
        {
            SignupResult result = Client.GetClient().SignUp(inputUser.Text, inputPass.Text, inputEmail.Text);

            if (result.success)
            {
                LoadingWindow.ShowAlert(this, "Signed up successfully");
                Thread.Sleep(1000);
                Finish();
            }
            else
            {
                LoadingWindow.ShowAlert(this, result.errorMessage);
                //LoadingWindow.ShowAlert(this, result.errorMessage;
            }
        }
        private void GotoSignIn(object o, EventArgs e)
        {
            Finish();
        }
    }
}