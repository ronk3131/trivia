﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace client
{
    [Activity(Label = "SignInWindow")]
    public class SignInWindow : Activity
    {
        TextView errorMessage;
        EditText inputUser, inputPass;
        Button signIn, gotoSignUp;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_login);
            errorMessage = FindViewById<TextView>(Resource.Id.login_errorMessage);
            inputUser = FindViewById<EditText>(Resource.Id.login_inputUser);
            inputPass = FindViewById<EditText>(Resource.Id.login_inputPass);
            signIn = FindViewById<Button>(Resource.Id.login_signIn);
            gotoSignUp = FindViewById<Button>(Resource.Id.login_signUp);

            signIn.Click += SignIn;
            gotoSignUp.Click += GotoSignUp; 
        }
        private void SignIn(object o, EventArgs e)
        {
            SigninResult result = Client.GetClient().SignIn(inputUser.Text, inputPass.Text);

            if (result.success)
            {
                LoadingWindow.ShowWindow<MainWindow>(this, true);
            }
            else
            {
                LoadingWindow.ShowAlert(this, result.errorMessage);
            }
        }
        private void GotoSignUp(object o, EventArgs e)
        {
            LoadingWindow.ShowWindow<SignUpWindow>(this, false);
        }
    }
}