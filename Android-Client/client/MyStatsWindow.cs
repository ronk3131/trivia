﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace client
{
    [Activity(Label = "MyStatsWindow")]
    public class MyStatsWindow : Activity
    {
        TextView gamesNumber, correctAnswers, wrongAnswers, avgTime;
        Button back;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_myStats);
            gamesNumber = FindViewById<TextView>(Resource.Id.myStats_gamesCount);
            correctAnswers = FindViewById<TextView>(Resource.Id.myStats_rightAnswers);
            wrongAnswers = FindViewById<TextView>(Resource.Id.myStats_wrongAnwers);
            avgTime = FindViewById<TextView>(Resource.Id.myStats_avgTime);
            back = FindViewById<Button>(Resource.Id.myStats_back);

            PerformancesResult performancesResult = Client.GetClient().GetPerformances();
            if (performancesResult.success)
            {
                gamesNumber.Text = "Number of games: " + performancesResult.gamesCount;
                correctAnswers.Text = "Correct answers: " + performancesResult.rightAnswersCount;
                wrongAnswers.Text = "Wrong answers: " + performancesResult.wrongAnswersCount;
                avgTime.Text = "Avarage time for answer: " + performancesResult.averageTimeForAnswer.ToString("0.3f");
            }
            back.Click += delegate { LoadingWindow.ShowWindow<MainWindow>(this, true); };
        }
    }
}