﻿using System;
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Content;
using Android.Runtime;
using Android.Widget;
using System.ComponentModel;
using System.Threading;
using Android.Net;

namespace client
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class LoadingWindow : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_loading);
            Handler handler = new Handler();
            BackgroundWorker connector = new BackgroundWorker();
            connector.DoWork += delegate
            {
                try
                {
                    Client.Connect(SERVER_IP, SERVER_PORT);
                    if (Client.GetClient().IsConnected())
                    {
                        Thread.Sleep(1000);
                        handler.Post(() => ShowWindow<SignInWindow>(this, true));
                    }
                }
                catch
                {
                    handler.Post(delegate
                    {
                        IsConnectedToWifi isConnectedToWifi = new IsConnectedToWifi(this);
                        RegisterReceiver(isConnectedToWifi, new IntentFilter("android.net.wifi.STATE_CHANGE"));
                    });
                }
            };
            connector.RunWorkerAsync();
        }

        public static void ShowWindow<MyWindow>(Activity thisActivity, bool finish) where MyWindow : Activity, new()
        {
            Intent nextWindow = new Intent(thisActivity, typeof(MyWindow));
            thisActivity.StartActivity(nextWindow);
            if (finish)
                thisActivity.Finish();
        }
        public static void ShowAlert(Activity thisActivity, string message)
        {
            Handler handler = new Handler();
            handler.Post(delegate
            {
                Android.App.AlertDialog.Builder dialog = new Android.App.AlertDialog.Builder(thisActivity);
                Android.App.AlertDialog alert = dialog.Create();
                alert.SetTitle("Error");
                alert.SetMessage(message);
                alert.SetIcon(Resource.Drawable.error);
                alert.SetCanceledOnTouchOutside(false);
                alert.SetButton("OK", (c, ev) => { });
                alert.Show();
            });
        }
        
        private static readonly string SERVER_IP = "192.168.1.31";
        private static readonly int SERVER_PORT = 8820;
    }
}