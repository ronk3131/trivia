﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace client
{
    [Activity(Label = "CreateRoomWindow")]
    public class CreateRoomWindow : Activity
    {
        EditText inputRoomName, inputMaxPlayers, inputQuestionsNumber, inputQuestionsTimeout;
        Button createRoom, back;
        TextView errorMessage;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_createRoom);

            inputRoomName = FindViewById<EditText>(Resource.Id.createRoom_inputRoomName);
            inputMaxPlayers = FindViewById<EditText>(Resource.Id.createRoom_inputMaxPlayers);
            inputQuestionsNumber = FindViewById<EditText>(Resource.Id.createRoom_inputQuestionsNumber);
            inputQuestionsTimeout = FindViewById<EditText>(Resource.Id.createRoom_inputQuestionsTimeout);
            createRoom = FindViewById<Button>(Resource.Id.createRoom_create);
            errorMessage = FindViewById<TextView>(Resource.Id.createRoom_errorMessage);
            back = FindViewById<Button>(Resource.Id.createRoom_back);

            back.Click += delegate { LoadingWindow.ShowWindow<MainWindow>(this, true); };
            createRoom.Click += CreateRoomOnClick;
        }
        private void CreateRoomOnClick(object o, EventArgs e)
        {
            try
            {
                CreateRoomResult result = Client.GetClient().CreateRoom(inputRoomName.Text, int.Parse(inputMaxPlayers.Text), int.Parse(inputQuestionsNumber.Text), double.Parse(inputQuestionsTimeout.Text));
                if (result.success)
                {
                    LoadingWindow.ShowWindow<RoomWindow>(this, true);
                }
                else
                {
                    LoadingWindow.ShowAlert(this, result.errorMessage);
                }
            }
            catch
            {
                /* Possible errors:
                 * user types in playersCount/questionCount a non integer text.
                 * user types in timeForAnswer a non double text.
                 */
                LoadingWindow.ShowAlert(this, "invalid inputs");
            }
        }
    }
}