﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace client
{
    [Activity(Label = "MainWindow")]
    public class MainWindow : Activity
    {
        TextView helloMsg;
        Button gotoJoinRoom, gotoCreateRoom, gotoMyStats;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            helloMsg = FindViewById<TextView>(Resource.Id.helloMsg);
            gotoJoinRoom = FindViewById<Button>(Resource.Id.main_gotoJoinRoom);
            gotoCreateRoom = FindViewById<Button>(Resource.Id.main_gotoCreateRoom);
            gotoMyStats = FindViewById<Button>(Resource.Id.main_gotoMyStats);

            helloMsg.Text = Client.GetClient().GetUsername();

            gotoJoinRoom.Click += delegate { LoadingWindow.ShowWindow<JoinRoomWIndow>(this, true); };
            gotoCreateRoom.Click += delegate { LoadingWindow.ShowWindow<CreateRoomWindow>(this, true); };
            gotoMyStats.Click += delegate { LoadingWindow.ShowWindow<MyStatsWindow>(this, true); };
        }
    }
}