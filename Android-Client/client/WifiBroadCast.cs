﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Net.Wifi;

namespace client
{
    [BroadcastReceiver]
    public class IsConnectedToWifi : BroadcastReceiver
    {
        Activity activity;
        public IsConnectedToWifi() {}
        public IsConnectedToWifi(Activity activity)
        {
            this.activity = activity;
        }
        public override void OnReceive(Context context, Intent intent)
        {
            Android.App.AlertDialog.Builder dialog = new Android.App.AlertDialog.Builder(activity);
            Android.App.AlertDialog alert = dialog.Create();
            alert.SetTitle("Error");
            alert.SetIcon(Resource.Drawable.error);
            alert.SetButton("CANCEL", (c, ev) => { Process.KillProcess(Process.MyPid()); });
            alert.SetCanceledOnTouchOutside(false);

            WifiManager wifiManager = (WifiManager)context.GetSystemService(Context.WifiService);
            WifiInfo wifiInfo = wifiManager.ConnectionInfo;

            if (wifiInfo.Frequency == -1)
                alert.SetMessage("Connect to wifi and try again");
            else
                alert.SetMessage("failed to connect to server");

            alert.Show();   
        }
    }
}