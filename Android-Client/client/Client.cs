﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace client
{
    enum RequestCode
    {
        None,
        Logout,
        Login,
        Signup,
        GetRooms,
        GetPlayersInRoom,
        JoinRoom,
        CreateRoom,
        MyPerformances,
        BestScores,
        StartGame,
        KickPlayer,
        GetRoomState,
        LeaveRoom,
        GetQuestion,
        SubmitAnswer,
        GetGameResults,
        LeaveGame,
        RoomSendMessage
    };
    enum ResponseCode
    {
        Error = 100,
        ProtocolFormat,
        Success = 200,
        UserTaken = 300,
        EmailTaken,
        InvalidPasswordAndUsername,
        AlreadyLoggedIn,
        InvalidUsername,
        InvalidPassword,
        InvalidEmail,
        RoomFull,
        RoomIsActive,
        InvalidRoomID,
        AlreadyInRoom,
        RoomPlayerLeaveJoin,
        RoomKickPlayer,
        CantKickAdmin,
        RoomClosed,
        GameStarted,
        InvalidCreateRoomArguementTypes,
        InvalidCreateRoomRoomName,
        InvalidCreateRoomMaxUsers,
        InvalidCreateRoomQuestionCount,
        InvalidCreateRoomAnswerTimeout,
        UserNotFound,
        SqlInjection,
        GameOver,
        WaitForPlayersToAnswer,
        PlayerNotFinished,
        AlreadySubmitted,
        QuestionNotAnswered,
        NewMessageUpdate
    };
    public enum UpdateRoomType
    {
        None,
        LeaveJoin,
        KickPlayer,
        StartGame,
        CloseRoom,
        NewMessage
    };


    public class Client
    {
        private static Client client;

        public Client(string serverAddr, int port)
        {
            _client = new TcpClient();
            var result = _client.BeginConnect(IPAddress.Parse(serverAddr), port, null, null);
            if (!result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(1)))
                throw new Exception();
            _stream = _client.GetStream();
        }

        public static void Connect(string serverAddr, int port)
        {
            client = new Client(serverAddr, port);
        }

        public static Client GetClient()
        {
            return client;
        }

        public bool IsConnected()
        {
            return _client.Connected;
        }
        public string GetUsername()
        {
            return _username;
        }
        public RoomMetaData GetCurrentRoom()
        {
            return _roomMetaData;
        }
        public bool IsRoomAdmin()
        {
            return _username == _roomMetaData.players[0];
        }

        public SigninResult SignIn(string username, string password)
        {
            SigninResult signinResult = new SigninResult();
            JObject json = new JObject
            {
                ["username"] = username,
                ["password"] = password
            };

            ServerResult serverResult = Communicate(RequestCode.Login, json);

            if (signinResult.success = serverResult.code == ResponseCode.Success)
            {
                _username = username;
            }
            signinResult.errorMessage = serverResult.json["message"].ToObject<string>();

            return signinResult;
        }
        public SignupResult SignUp(string username, string password, string email)
        {
            SignupResult signupResult = new SignupResult();
            JObject json = new JObject
            {
                ["username"] = username,
                ["password"] = password,
                ["email"] = email
            };

            ServerResult serverResult = Communicate(RequestCode.Signup, json);

            signupResult.success = serverResult.code == ResponseCode.Success;
            signupResult.errorMessage = serverResult.json["message"].ToObject<string>();

            return signupResult;
        }

        public void Signout()
        {
            Communicate(RequestCode.Logout, null, false);
            _username = "";
        }
        public PerformancesResult GetPerformances()
        {
            PerformancesResult performances = new PerformancesResult();

            ServerResult serverResult = Communicate(RequestCode.MyPerformances, null);

            if (performances.success = serverResult.code == ResponseCode.Success)
            {
                performances.gamesCount = Convert.ToInt32(serverResult.json["gamesCount"]);
                performances.rightAnswersCount = Convert.ToInt32(serverResult.json["rightAnswersCount"]);
                performances.wrongAnswersCount = Convert.ToInt32(serverResult.json["wrongAnswersCount"]);
                performances.averageTimeForAnswer = Convert.ToDouble(serverResult.json["averageTimeForAnswer"]);
            }
            else
            {
                performances.errorMessage = serverResult.json["message"].ToObject<string>();
            }
            return performances;
        }
        public BestScoresResult BestScores()
        {
            BestScoresResult bestScoresResult = new BestScoresResult();

            ServerResult serverResult = Communicate(RequestCode.BestScores, null);

            if (!(bestScoresResult.success = serverResult.code == ResponseCode.Success))
            {
                bestScoresResult.errorMessage = serverResult.json["message"].ToObject<string>();
            }
            else
            {
                int usersCount = serverResult.json["usersCount"].ToObject<int>();

                bestScoresResult.users = new List<Tuple<string, int>>();
                for (int i = 0; i < usersCount; i++)
                {
                    bestScoresResult.users.Add(new Tuple<string, int>(serverResult.json["username" + i].ToObject<string>(), serverResult.json["score" + i].ToObject<int>()));
                }
            }

            return bestScoresResult;
        }
        public CreateRoomResult CreateRoom(string roomName, int maxPlayers, int questionsCount, double timeForAnswer)
        {
            CreateRoomResult createRoomResult = new CreateRoomResult();

            JObject json = new JObject
            {
                ["roomName"] = roomName,
                ["maxUsers"] = maxPlayers,
                ["questionCount"] = questionsCount,
                ["answerTimeout"] = timeForAnswer
            };

            ServerResult serverResult = Communicate(RequestCode.CreateRoom, json);

            if (createRoomResult.success = serverResult.code == ResponseCode.Success)
            {
                _roomMetaData.name = roomName;
            }
            else
            {
                createRoomResult.errorMessage = serverResult.json["message"].ToObject<string>();
            }
            return createRoomResult;
        }
        public GetRoomsResult GetRooms()
        {
            GetRoomsResult getRoomsResult = new GetRoomsResult();

            ServerResult serverResult = Communicate(RequestCode.GetRooms, null);

            if (!(getRoomsResult.success = serverResult.code == ResponseCode.Success))
            {
                getRoomsResult.errorMessage = serverResult.json["message"].ToObject<string>();
            }
            else
            {
                int roomsCount = serverResult.json["roomCount"].ToObject<int>();
                getRoomsResult.rooms = new Dictionary<int, string>();
                for (int i = 0; i < roomsCount; i++)
                {
                    // every room in the list is sent in the format: "id,name"
                    string[] roomData = serverResult.json[Convert.ToString(i)].ToObject<string>().Split(',');
                    getRoomsResult.rooms[int.Parse(roomData[0])] = roomData[1];
                }
            }
            return getRoomsResult;
        }
        public GetPlayersInRoomResult GetPlayersInRoom(int roomId)
        {
            GetPlayersInRoomResult getPlayersResult = new GetPlayersInRoomResult();

            JObject json = new JObject
            {
                ["roomId"] = roomId
            };

            ServerResult serverResult = Communicate(RequestCode.GetPlayersInRoom, json);

            if (!(getPlayersResult.success = serverResult.code == ResponseCode.Success))
            {
                getPlayersResult.errorMessage = serverResult.json["message"].ToObject<string>();
            }
            else
            {
                getPlayersResult.players = GetPlayersListFromJson(serverResult.json);
            }
            return getPlayersResult;
        }
        public JoinRoomResult JoinRoom(int roomId, string roomName)
        {
            JoinRoomResult joinRoomResult = new JoinRoomResult();

            JObject json = new JObject
            {
                ["roomId"] = roomId
            };

            ServerResult serverResult = Communicate(RequestCode.JoinRoom, json);

            if (joinRoomResult.success = serverResult.code == ResponseCode.Success)
            {
                _roomMetaData.name = roomName;
            }
            else
            {
                joinRoomResult.errorMessage = serverResult.json["message"].ToObject<string>();
            }
            return joinRoomResult;
        }
        public KickPlayerResult KickPlayer(string playerName)
        {
            KickPlayerResult kickPlayerResult = new KickPlayerResult();

            JObject json = new JObject
            {
                ["playerName"] = playerName
            };

            ServerResult serverResult = Communicate(RequestCode.KickPlayer, json);

            if (!(kickPlayerResult.success = serverResult.code == ResponseCode.Success))
            {
                kickPlayerResult.errorMessage = serverResult.json["message"].ToObject<string>();
            }
            return kickPlayerResult;
        }

        public GetRoomInfoResult GetRoomInfo()
        {
            GetRoomInfoResult getRoomInfoResult = new GetRoomInfoResult();

            ServerResult serverResult = Communicate(RequestCode.GetRoomState, null);

            if (!(getRoomInfoResult.success = serverResult.code == ResponseCode.Success))
            {
                getRoomInfoResult.errorMessage = serverResult.json["message"].ToObject<string>();
            }
            else
            {
                getRoomInfoResult.room.players = GetPlayersListFromJson(serverResult.json);
                getRoomInfoResult.room.name = serverResult.json["roomName"].ToObject<string>();
                getRoomInfoResult.room.answerTimeout = serverResult.json["answerTimeout"].ToObject<double>();
                getRoomInfoResult.room.questionCount = serverResult.json["questionCount"].ToObject<int>();
                getRoomInfoResult.room.maxPlayersCount = serverResult.json["maxPlayersCount"].ToObject<int>();
                _roomMetaData = getRoomInfoResult.room;
            }
            return getRoomInfoResult;
        }
        public RoomUpdateResult GetRoomUpdate(ref bool isCancelled, ref bool isBlocked)
        {
            ServerResult serverResult = RoomUpdateCommunicate(ref isCancelled, ref isBlocked);
            RoomUpdateResult roomUpdate;
            
            roomUpdate.playerName = null;
            roomUpdate.message = null;
            roomUpdate.updateType = UpdateRoomType.None;

            switch (serverResult.code)
            {
                case ResponseCode.RoomClosed:
                    roomUpdate.updateType = UpdateRoomType.CloseRoom;
                    break;
                case ResponseCode.RoomPlayerLeaveJoin:
                    roomUpdate.updateType = UpdateRoomType.LeaveJoin;
                    roomUpdate.playerName = serverResult.json["playerName"].ToObject<string>();
                    break;
                case ResponseCode.GameStarted:
                    roomUpdate.updateType = UpdateRoomType.StartGame;
                    break;
                case ResponseCode.RoomKickPlayer:
                    roomUpdate.updateType = UpdateRoomType.KickPlayer;
                    roomUpdate.playerName = serverResult.json["playerName"].ToObject<string>();
                    break;
                case ResponseCode.NewMessageUpdate:
                    roomUpdate.updateType = UpdateRoomType.NewMessage;
                    roomUpdate.playerName = serverResult.json["playerName"].ToObject<string>();
                    roomUpdate.message = serverResult.json["message"].ToObject<string>();
                    break;
            }
            return roomUpdate;
        }
        public void LeaveRoom()
        {
            Communicate(RequestCode.LeaveRoom, null, false);
        }
        public void StartGame()
        {
            Communicate(RequestCode.StartGame, null, false);
        }
        public void LeaveGame()
        {
            Communicate(RequestCode.LeaveGame, null, false);
        }
        public void SendMessage(string message)
        {
            JObject json = new JObject
            {
                ["message"] = message
            };
            Communicate(RequestCode.RoomSendMessage, json, false);
        }

        public GetQuestionResult GetNextQuestion()
        {
            GetQuestionResult getQuestionResult = new GetQuestionResult();

            ServerResult serverResult = Communicate(RequestCode.GetQuestion, null);

            getQuestionResult.success = serverResult.code == ResponseCode.Success;
            if (serverResult.code == ResponseCode.Success)
            {
                getQuestionResult.question = GetQuestionFromJson(serverResult.json);
            }
            else
            {
                getQuestionResult.errorMessage = serverResult.json["message"].ToObject<string>();
            }

            return getQuestionResult;
        }
        public SubmitAnswerResult SubmitAnswer(int answerId)
        {
            SubmitAnswerResult submitAnswerResult = new SubmitAnswerResult();
            JObject json = new JObject
            {
                ["answerId"] = answerId
            };

            ServerResult serverResult = Communicate(RequestCode.SubmitAnswer, json);

            if (submitAnswerResult.success = serverResult.code == ResponseCode.Success)
            {
                submitAnswerResult.correctAnswer = serverResult.json["correctAnswer"].ToObject<int>();
            }

            return submitAnswerResult;
        }
        public GetGameResultsResult GetGameResults()
        {
            GetGameResultsResult getGameResultsResult = new GetGameResultsResult();

            ServerResult serverResult = Communicate(RequestCode.GetGameResults, null);

            if (getGameResultsResult.success = serverResult.code == ResponseCode.Success)
            {
                getGameResultsResult.playerResults = new List<PlayerResults>();
                for (int i = 0; i < serverResult.json["playersCount"].ToObject<int>(); i++)
                {
                    // name,correct
                    string[] playerResultsParts = serverResult.json[Convert.ToString(i)].ToObject<string>().Split(',');
                    PlayerResults playerResults = new PlayerResults
                    {
                        username = playerResultsParts[0],
                        score = int.Parse(playerResultsParts[1])
                    };

                    getGameResultsResult.playerResults.Add(playerResults);
                }
            }
            else
            {
                getGameResultsResult.errorMessage = serverResult.json["message"].ToObject<string>();
            }

            return getGameResultsResult;
        }

        private ServerResult Communicate(RequestCode requestCode, JObject requestJson, bool expectResponse = true)
        {
            ServerResult result = new ServerResult();

            // send message to the server
            if (requestCode != RequestCode.None)
            {
                byte[] request = ConvertToBytes(requestCode, requestJson);
                _stream.Write(request, 0, request.Length);
                _stream.Flush();
            }

            if (expectResponse)
            {
                // receive the server's response
                byte[] buffer;

                //get response code
                buffer = new byte[CODE_ID_SIZE];
                _stream.Read(buffer, 0, buffer.Length);
                result.code = (ResponseCode)(int.Parse(ASCIIEncoding.ASCII.GetString(buffer)));

                //get data length
                buffer = new byte[DATA_LEN_SIZE];
                _stream.Read(buffer, 0, buffer.Length);
                int dataLen = int.Parse(ASCIIEncoding.ASCII.GetString(buffer));

                if (dataLen > 0)
                {
                    //get data
                    buffer = new byte[dataLen];
                    _stream.Read(buffer, 0, buffer.Length);
                    result.json = JObject.Parse(ASCIIEncoding.ASCII.GetString(buffer));
                }
            }
            return result;
        }

        private ServerResult RoomUpdateCommunicate(ref bool isCancelled, ref bool isBlocked)
        {
            ServerResult result = new ServerResult();

            // receive the server's response
            byte[] buffer;

            //get response code
            while (!_stream.DataAvailable || isBlocked)
            {
                if(isCancelled)
                {
                    throw new Exception();
                }
            }
            buffer = new byte[CODE_ID_SIZE];
            _stream.Read(buffer, 0, buffer.Length);
            result.code = (ResponseCode)(int.Parse(ASCIIEncoding.ASCII.GetString(buffer)));

            //get data length
            while (!_stream.DataAvailable)
            {
                if (isCancelled)
                {
                    throw new Exception();
                }
            }
            buffer = new byte[DATA_LEN_SIZE];
            _stream.Read(buffer, 0, buffer.Length);
            int dataLen = int.Parse(ASCIIEncoding.ASCII.GetString(buffer));

            if (dataLen > 0)
            {
                //get data
                while (!_stream.DataAvailable)
                {
                    if (isCancelled)
                    {
                        throw new Exception();
                    }
                }
                buffer = new byte[dataLen];
                _stream.Read(buffer, 0, buffer.Length);
                result.json = JObject.Parse(ASCIIEncoding.ASCII.GetString(buffer));
            }
            return result;
        }

        private byte[] ConvertToBytes(RequestCode requestCode, JObject requestJson)
        {
            string requestStr = ((int)requestCode).ToString().PadLeft(CODE_ID_SIZE, '0');
            string jsonStr = requestJson?.ToString(Formatting.None) ?? "";
            requestStr += jsonStr.Length.ToString().PadLeft(DATA_LEN_SIZE, '0') + jsonStr;
            return ASCIIEncoding.ASCII.GetBytes(requestStr);
        }
        private List<string> GetPlayersListFromJson(JObject json)
        {
            int playersCount = json["playersCount"].ToObject<int>();
            List<string> players = new List<string>();
            for (int i = 0; i < playersCount; i++)
            {
                players.Add(json[Convert.ToString(i)].ToObject<string>());
            }
            return players;
        }
        private Question GetQuestionFromJson(JObject json)
        {
            Question question = new Question
            {
                question = json["question"].ToObject<string>(),
                answers = new string[ANSWERS_COUNT]
            };
            for (int i = 0; i < ANSWERS_COUNT; i++)
            {
                question.answers[i] = json[Convert.ToString(i)].ToObject<string>();
            }
            return question;
        }

        private const int CODE_ID_SIZE = 3;
        private const int DATA_LEN_SIZE = 4;
        private const int ANSWERS_COUNT = 4;
        
        private TcpClient _client;
        private NetworkStream _stream;
        private string _username;
        private RoomMetaData _roomMetaData;

        private struct ServerResult
        {
            public ResponseCode code;
            public JObject json;
        }
    }
    public struct RoomUpdateResult
    {
        public UpdateRoomType updateType;
        public string playerName;
        public string message;
    }
    public struct SigninResult
    {
        public bool success;
        public string errorMessage;
    }
    public struct SignupResult
    {
        public bool success;
        public string errorMessage;
    }
    public struct CreateRoomResult
    {
        public bool success;
        public string errorMessage;
    }
    public struct JoinRoomResult
    {
        public bool success;
        public string errorMessage;
    }
    public struct KickPlayerResult
    {
        public bool success;
        public string errorMessage;
    }
    public struct PerformancesResult
    {
        public bool success;
        public string errorMessage;
        public int gamesCount;
        public int rightAnswersCount;
        public int wrongAnswersCount;
        public double averageTimeForAnswer;
    }
    public struct BestScoresResult
    {
        public bool success;
        public string errorMessage;
        public List<Tuple<string, int>> users;
    }
    public struct GetRoomsResult
    {
        public bool success;
        public string errorMessage;
        public Dictionary<int, string> rooms;
    }
    public struct GetPlayersInRoomResult
    {
        public bool success;
        public string errorMessage;
        public List<string> players;
    }
    public struct GetRoomInfoResult
    {
        public bool success;
        public string errorMessage;
        public RoomMetaData room;
    }
    public struct GetQuestionResult
    {
        public bool success;
        public string errorMessage;
        public Question question;
    }
    public struct SubmitAnswerResult
    {
        public bool success;
        public int correctAnswer;
    }
    public struct GetGameResultsResult
    {
        public bool success;
        public string errorMessage;
        public List<PlayerResults> playerResults; 
    }

    public struct Question
    {
        public string question;
        public string[] answers;
    }
    public struct RoomMetaData
    {
        public List<string> players;
        public int questionCount;
        public double answerTimeout;
        public int maxPlayersCount;
        public string name;
    }
    public struct PlayerResults
    {
        public string username;
        public int score;
    }
}